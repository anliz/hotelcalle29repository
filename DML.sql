

INSERT INTO TIPO_HABITACIONES (codigo, descripcion, tarifa1, estado) VALUES (01,'STANDARD VENTILADOR',50000,TRUE);
INSERT INTO TIPO_HABITACIONES (codigo, descripcion, tarifa2, estado) VALUES (02,'STANDARD A.A.',80000,TRUE);
INSERT INTO TIPO_HABITACIONES (codigo, descripcion, tarifa3, estado) VALUES (03,'SUITE',100000,TRUE);

INSERT INTO ELEMENTOS (codigo, descripcion, cantidad, valor_alquiler, disponible, estado) VALUES (01,'Sillas',2000,500,2000, TRUE);
INSERT INTO ELEMENTOS (codigo, descripcion, cantidad, valor_alquiler, disponible, estado) VALUES (02,'Mesas',1000,1500,1000, TRUE);
INSERT INTO ELEMENTOS (codigo, descripcion, cantidad, valor_alquiler, disponible, estado) VALUES (03,'Microfonos',20,50000,20, TRUE);
INSERT INTO ELEMENTOS (codigo, descripcion, cantidad, valor_alquiler, disponible, estado) VALUES (04,'video beams',15,80000,15, TRUE);
INSERT INTO ELEMENTOS (codigo, descripcion, cantidad, valor_alquiler, disponible, estado) VALUES (05,'computadoras portátiles',10,100000,10, TRUE);

INSERT INTO SALA_EVENTOS (numero, numero_planta, descripcion, tarifa_hora, estado) VALUES (1,01,'Sala Eventos 01',50000,TRUE);
INSERT INTO SALA_EVENTOS (numero, numero_planta, descripcion, tarifa_hora, estado) VALUES (2,01,'Sala Eventos 02',50000,TRUE);
INSERT INTO SALA_EVENTOS (numero, numero_planta, descripcion, tarifa_hora, estado) VALUES (3,02,'Sala Eventos principal',100000,TRUE);
INSERT INTO SALA_EVENTOS (numero, numero_planta, descripcion, tarifa_hora, estado) VALUES (4,03,'Sala Eventos 04',50000,TRUE);
INSERT INTO SALA_EVENTOS (numero, numero_planta, descripcion, tarifa_hora, estado) VALUES (5,03,'Sala Eventos 05',50000,TRUE);
INSERT INTO SALA_EVENTOS (numero, numero_planta, descripcion, tarifa_hora, estado) VALUES (6,03,'Sala Eventos 06',50000,TRUE);
INSERT INTO SALA_EVENTOS (numero, numero_planta, descripcion, tarifa_hora, estado) VALUES (7,04,'Sala Eventos 07',100000,TRUE);

INSERT INTO HABITACIONES (numero, numero_planta, num_cama_sencilla, num_cama_doble, estado_ocupacion, codigo_tipo_habitacion, estado) 
	VALUES (1,01,2,1,'DISPONIBLE',01,TRUE);
INSERT INTO HABITACIONES (numero, numero_planta, num_cama_sencilla, num_cama_doble, estado_ocupacion, codigo_tipo_habitacion, estado) 
	VALUES (2,01,3,1,'DISPONIBLE',01,TRUE);
INSERT INTO HABITACIONES (numero, numero_planta, num_cama_sencilla, num_cama_doble, estado_ocupacion, codigo_tipo_habitacion, estado) 
	VALUES (3,01,0,1,'DISPONIBLE',02,TRUE);
INSERT INTO HABITACIONES (numero, numero_planta, num_cama_sencilla, num_cama_doble, estado_ocupacion, codigo_tipo_habitacion, estado) 
	VALUES (4,01,0,1,'DISPONIBLE',02,TRUE);
INSERT INTO HABITACIONES (numero, numero_planta, num_cama_sencilla, num_cama_doble, estado_ocupacion, codigo_tipo_habitacion, estado) 
	VALUES (5,01,0,1,'DISPONIBLE',02,TRUE);
INSERT INTO HABITACIONES (numero, numero_planta, num_cama_sencilla, num_cama_doble, estado_ocupacion, codigo_tipo_habitacion, estado) 
	VALUES (6,01,0,1,'DISPONIBLE',03,TRUE);
INSERT INTO HABITACIONES (numero, numero_planta, num_cama_sencilla, num_cama_doble, estado_ocupacion, codigo_tipo_habitacion, estado) 
	VALUES (7,01,2,1,'DISPONIBLE',02,TRUE);
INSERT INTO HABITACIONES (numero, numero_planta, num_cama_sencilla, num_cama_doble, estado_ocupacion, codigo_tipo_habitacion, estado) 
	VALUES (8,01,1,2,'DISPONIBLE',02,TRUE);
INSERT INTO HABITACIONES (numero, numero_planta, num_cama_sencilla, num_cama_doble, estado_ocupacion, codigo_tipo_habitacion, estado) 
	VALUES (9,01,1,2,'DISPONIBLE',01,TRUE);
INSERT INTO HABITACIONES (numero, numero_planta, num_cama_sencilla, num_cama_doble, estado_ocupacion, codigo_tipo_habitacion, estado) 
	VALUES (10,01,0,2,'DISPONIBLE',03,TRUE);
INSERT INTO HABITACIONES (numero, numero_planta, num_cama_sencilla, num_cama_doble, estado_ocupacion, codigo_tipo_habitacion, estado) 
	VALUES (11,01,1,1,'DISPONIBLE',02,TRUE);

	
INSERT INTO TIPO_PAGOS(id, descripcion, estado) VALUES (1,'EFECTIVO',TRUE);
INSERT INTO TIPO_PAGOS(id, descripcion, estado) VALUES (2,'DEBITO',TRUE);
INSERT INTO TIPO_PAGOS(id, descripcion, estado) VALUES (3,'CREDITO',TRUE);
INSERT INTO TIPO_PAGOS(id, descripcion, estado) VALUES (4,'CHEQUE',TRUE);

INSERT INTO PERSONAS(documento,nombre,apellido,fecha_nacimiento,direccion,telefono) 
	VALUES (1094373860,'Sara Maria','Zanabria Perez','16/07/1990','Av 28 # 33-44 Prados Este','3213456789');
INSERT INTO PERSONAS(documento,nombre,apellido,fecha_nacimiento,direccion,telefono) 
	VALUES (1094353457,'Sandra Milena','Caceres Florez','22/04/1993','Cll 35 # 32-23 San Jose','3213346436 - 3234394534');
INSERT INTO PERSONAS(documento,nombre,apellido,fecha_nacimiento,direccion,telefono) 
	VALUES (1093356642,'Jeronimo','Salazar Carrillo','28/02/1995','Av 0 # 11-23 Centro','3233464432');
INSERT INTO PERSONAS(documento,nombre,apellido,fecha_nacimiento,direccion,telefono) 
	VALUES (1090353361,'Francisco Jose','Arenales Gaitan','30/08/1988','Av 4 # 12-34 Villa Nueva','3133424432');
INSERT INTO PERSONAS(documento,nombre,apellido,fecha_nacimiento,direccion,telefono) 
	VALUES (13445501,'Erasmo Emilio','Lizarazo Gamboa','07/11/1961','Cll 29 # 26-75 Belen','3112046958');
INSERT INTO PERSONAS(documento,nombre,apellido,fecha_nacimiento,direccion,telefono) 
	VALUES (1090383368,'Wilfred Uriel','Garcia','12/07/1989','Cll 29 # 26-75 Belen','3219893104');
INSERT INTO PERSONAS(documento,nombre,apellido,fecha_nacimiento,direccion,telefono) 
	VALUES (1093356645,'Samanta','Garcia Salazar','28/12/1993','Av 0 # 10-25 Centro','3233227684');



INSERT INTO EMPLEADOS(documento, cargo, estado) VALUES (1090383368,'RECEPCIONISTA',TRUE);
INSERT INTO EMPLEADOS(documento, cargo, estado) VALUES (1093356645,'RECEPCIONISTA',TRUE);
INSERT INTO EMPLEADOS(documento, cargo, estado) VALUES (1094353457,'CAMARERA',TRUE);
INSERT INTO EMPLEADOS(documento, cargo, estado) VALUES (1093356642,'BOTONES',TRUE);
INSERT INTO EMPLEADOS(documento, cargo, estado) VALUES (1090353361,'AUXILIAR CONTABLE',TRUE);
INSERT INTO EMPLEADOS(documento, cargo, estado) VALUES (13445501,'JEFE DE COMPRAS',TRUE);
INSERT INTO EMPLEADOS(documento, cargo, estado) VALUES (1094373860,'ADMINISTRADOR',TRUE);


INSERT INTO SERVICIO_ADICIONALES(codigo,descripcion,valor,estado)
VALUES(01,'Restaurante',50000,true);

INSERT INTO SERVICIO_ADICIONALES(codigo,descripcion,valor,estado)
VALUES(02,'Lavandería',10000,true);

INSERT INTO SERVICIO_ADICIONALES(codigo,descripcion,valor,estado)
VALUES(03,'Desayuno a la habitación',30000,true);

INSERT INTO SERVICIO_ADICIONALES(codigo,descripcion,valor,estado)
VALUES(04,'Llamadas a larga distacia',10000,true);








