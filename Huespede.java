package model;

import java.util.Date;

public class Huespede extends Persona {
	private boolean estado; 

    public Huespede(boolean estado, int documento, String nombre, String apellido, Date fecha_nacimiento, String direccion, String telefono, String email) {
        super(documento, nombre, apellido, fecha_nacimiento, direccion, telefono, email);
        this.estado = estado;
    }

    public boolean isEstado() {
        return this.estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }



}
