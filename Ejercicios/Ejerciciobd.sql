
--1. crearle una reserva de ejemplo, asociarle habitaciones de ejemplo a la reserva.

INSERT INTO RESERVAS(codigo,fecha,fecha_entrada,fecha_salida,cantidad_personas,estado,documento_cliente,id_usuario,tipo_tarifa)
VALUES (01,'26/07/2021','28/07/2021','01/08/2021',4,true,1092359653,'hermoso','Normal');
	
INSERT INTO RESERVA_HABITACIONES(id,estado,numero_habitacion,codigo_reserva) VALUES (01,true,07,01);

--2. completar los datos del cliente

INSERT INTO PERSONAS(documento,nombre,apellido,fecha_nacimiento,direccion,telefono,email) 
VALUES (1092359653,'Angelica','Lizarazo','21/12/1995','Av 0 # 10-25 Centro','3185364482','ange0916,al@gmail.com');
	
INSERT INTO CLIENTES(documento,lugar_expedicion,ciudad,pais,ocupacion,estado) 
VALUES(1092359653,'Villa Rosario','Cucuta','Colombia','Desarrolladora',true);

INSERT INTO HUESPEDES(documento,estado) VALUES (1092359653,true);

--3. agregar las 3 personas que la acompañan para llevar el registro en el Hotel.	

INSERT INTO PERSONAS(documento,nombre,apellido,fecha_nacimiento,direccion,telefono,email) 
VALUES (1094983023,'Angie','Lizarazo','12/9/1999','Av 0 # 10-25 Centro','3185364482','anli912@gmail.com');
INSERT INTO PERSONAS(documento,nombre,apellido,fecha_nacimiento,direccion,telefono,email) 
VALUES (1094786907,'Angel','Lizarazo','17/02/2007','Av 0 # 10-25 Centro','3185364482','angellizarazo@gmail.com');		
INSERT INTO PERSONAS(documento,nombre,apellido,fecha_nacimiento,direccion,telefono,email) 
VALUES (1090768774,'Lariza','Lizarazo','22/02/1997','Av 0 # 10-25 Centro','3185364482','larizalizarazo@gmail.com');	

INSERT INTO HUESPEDES(documento,estado) VALUES (1094983023,true);	
INSERT INTO HUESPEDES(documento,estado) VALUES (1094786907,true);
INSERT INTO HUESPEDES(documento,estado) VALUES (1090768774,true);

INSERT INTO CUENTA_HUESPEDE_HABITACIONES( id, estado,numero_cuenta,numero_habitacion,documento_huespede)
VALUES(01,true,01,07,1094983023);
INSERT INTO CUENTA_HUESPEDE_HABITACIONES( id, estado,numero_cuenta,numero_habitacion,documento_huespede)
VALUES(02,true,01,07,1094786907);
INSERT INTO CUENTA_HUESPEDE_HABITACIONES( id, estado,numero_cuenta,numero_habitacion,documento_huespede)
VALUES(03,true,01,07,1090768774);
INSERT INTO CUENTA_HUESPEDE_HABITACIONES( id, estado,numero_cuenta,numero_habitacion,documento_huespede)
VALUES(04,true,01,07,1092359653);

--4. Lo atenderá el empleado WILFRED GARCIA (crearle datos de prueba), que tiene como usuario/clave en el sistema hermoso/hermoso14.

INSERT INTO USUARIOS(id,documento_cliente,contrasena,documento_empleado)
VALUES('hermoso',1092359653,'hermoso14',1090383368);

--5. Ademas angelica dará una conferencia, por lo que alquilará la sala1, para su evento, con una duración de 5 horas, y agregando elementos de ejemplos.

INSERT INTO ALQUILER_SALA_EVENTOS(numero,fecha,hora_llegada,hora_salida,confirmar,estado,num_sala_evento,numero_cuenta,documento_cliente)
VALUES(01,'29/07/2021','14:30','19:30',true,true,1,01,1092359653);

INSERT INTO ALQUILER_SALA_EVENTO_ELEMENTOS(id,estado,codigo_elemento, numero_alquiler,cantidad)
VALUES(01,true,1,01,80);

INSERT INTO ALQUILER_SALA_EVENTO_ELEMENTOS(id,estado,codigo_elemento, numero_alquiler,cantidad)
VALUES(02,true,3,01,2);
	
INSERT INTO ALQUILER_SALA_EVENTO_ELEMENTOS(id,estado,codigo_elemento, numero_alquiler,cantidad)
VALUES(03,true,4,01,1);

--6. Angelica se presenta en el Hotel el día 28/07/2021 y confirma la reserva. Por tanto se le abre la cuenta.

INSERT INTO CUENTAS(numero,fecha_apertura,fecha_cierre,cantidad_personas,cantidad_dias,tipo_tarifa,observaciones,abono,estado_cuenta,estado,id_usuario,documento_cliente,codigo_reserva)
VALUES(01,'28/07/2021','01/08/2021',4,5,'Normal','Sin observaciones',400000,'Abierta',true,'hermoso',1092359653,01);

INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(01,'Servicio habitacion',1,80000,'28/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(02,'Servicio habitacion',1,80000,'29/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(03,'Servicio habitacion',1,80000,'30/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(04,'Servicio habitacion',1,80000,'31/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(05,'Servicio habitacion',1,80000,'01/08/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(06,'Alquiler de sala eventos',1,250000,'29/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(07,'Alquiler de elementos sillas',80,40000,'29/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(08,'Alquiler de elementos Microfonos',2,100000,'29/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(09,'Alquiler de elementos Video beams',1,80000,'29/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(10,'Servicio de desayuno a la habitacion',1,30000,'29/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(11,'Servicio de desayuno a la habitacion',1,30000,'30/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(12,'Servicio de desayuno a la habitacion',1,30000,'31/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(13,'Servicio de desayuno a la habitacion',1,30000,'01/08/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(14,'Servicio de restaurante',1,50000,'28/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(15,'Servicio de restaurante',1,50000,'29/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(16,'Servicio de restaurante',1,50000,'30/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(17,'Servicio de restaurante',1,50000,'31/07/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(18,'Servicio de restaurante',1,50000,'01/08/2021',true,01);
INSERT INTO CUENTA_DETALLES(numero,descripcion,cantidad,valor,fecha,estado,numero_cuenta)
VALUES(19,'Servicio de lavanderia',1,10000,'31/07/2021',true,01);



--7. Angelica indica que trabaja en la empresa Topgroup con NIT 6-00586241-5

INSERT INTO	EMPRESAS(id, nit,razon_social,direccion,telefono,ciudad,pais,estado)
VALUES(01,'6-00586241-5','Topgroup','Av 2 # 12-88 centro','5769834 - 3123934627','Cucuta','Colombia',true);

INSERT INTO EMPRESA_CLIENTES( id,documento_cliente,id_empresa,estado)
VALUES(01,1092359653,01,true);


--8. Angelica utiliza el servicio de restaurante los 5 dias de su estadia, ademas utiliza el servicio de lavanderia (1 vez) y servicio desayuno a la cama los 4 días.

INSERT INTO COMANDAS(codigo,fecha,hora_comanda,descripcion,valor,estado,numero_cuenta,cod_servicio_adicional,id_usuario)
VALUES (01,'28/07/2021','12:30','Servicio de restaurante',50000,true,01,01,'hermoso');
INSERT INTO COMANDAS(codigo,fecha,hora_comanda,descripcion,valor,estado,numero_cuenta,cod_servicio_adicional,id_usuario)
VALUES (02,'29/07/2021','13:00','Servicio de restaurante',50000,true,01,01,'hermoso');
INSERT INTO COMANDAS(codigo,fecha,hora_comanda,descripcion,valor,estado,numero_cuenta,cod_servicio_adicional,id_usuario)
VALUES (03,'30/07/2021','13:00','Servicio de restaurante',50000,true,01,01,'hermoso');
INSERT INTO COMANDAS(codigo,fecha,hora_comanda,descripcion,valor,estado,numero_cuenta,cod_servicio_adicional,id_usuario)
VALUES (04,'31/07/2021','12:30','Servicio de restaurante',50000,true,01,01,'hermoso');
INSERT INTO COMANDAS(codigo,fecha,hora_comanda,descripcion,valor,estado,numero_cuenta,cod_servicio_adicional,id_usuario)
VALUES (05,'01/08/2021','12:30','Servicio de restaurante',50000,true,01,01,'hermoso');

INSERT INTO COMANDAS(codigo,fecha,hora_comanda,descripcion,valor,estado,numero_cuenta,cod_servicio_adicional,id_usuario)
VALUES (06,'31/07/2021','08:30','Servicio de lavanderia',10000,true,01,02,'hermoso');

INSERT INTO COMANDAS(codigo,fecha,hora_comanda,descripcion,valor,estado,numero_cuenta,cod_servicio_adicional,id_usuario)
VALUES (07,'29/07/2021','07:30','Servicio de desayuno a la habitacion',30000,true,01,03,'hermoso');
INSERT INTO COMANDAS(codigo,fecha,hora_comanda,descripcion,valor,estado,numero_cuenta,cod_servicio_adicional,id_usuario)
VALUES (08,'30/07/2021','08:00','Servicio de desayuno a la habitacion',30000,true,01,03,'hermoso');
INSERT INTO COMANDAS(codigo,fecha,hora_comanda,descripcion,valor,estado,numero_cuenta,cod_servicio_adicional,id_usuario)
VALUES (09,'31/07/2021','08:00','Servicio de desayuno a la habitacion',30000,true,01,03,'hermoso');
INSERT INTO COMANDAS(codigo,fecha,hora_comanda,descripcion,valor,estado,numero_cuenta,cod_servicio_adicional,id_usuario)
VALUES (10,'01/08/2021','07:30','Servicio de desayuno a la habitacion',30000,true,01,03,'hermoso');

--10. pasados los 5 días, angelica solicita el cierre de su cuenta, y pide la factura con los detalles.

INSERT INTO FACTURAS(numero,fecha,total,estado,observaciones,numero_cuenta,id_tipo_pago,documento_cliente)
VALUES(01,'01/08/2021',850000,true,'Ninguna',01,2,1092359653);


INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(01,'Servicio habitacion',1,80000,68966,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(02,'Servicio habitacion',1,80000,68966,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(03,'Servicio habitacion',1,80000,68966,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(04,'Servicio habitacion',1,80000,68966,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(05,'Servicio habitacion',1,80000,68966,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(06,'Alquiler de sala eventos',1,80000,250000,215517,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(07,'Alquiler de elementos sillas',80,40000,34482,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(08,'Alquiler de elementos Microfonos',2,100000,86206,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(09,'Alquiler de elementos Video beams',1,30000,68966,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(10,'Servicio de desayuno a la habitacion ',1,30000,25862,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(11,'Servicio de desayuno a la habitacion',1,30000,25862,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(12,'Servicio de desayuno a la habitacion',1,30000,25862,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(13,'Servicio de desayuno a la habitacion',1,30000,25862,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(14,'Servicio de restaurante',1,50000,43103,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(15,'Servicio de restaurante',1,50000,43103,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(16,'Servicio de restaurante',1,50000,43103,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(17,'Servicio de restaurante',1,50000,43103,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(18,'Servicio de restaurante',1,50000,43103,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(19,'Servicio de lavanderia',1,100000,86206,true,01);
INSERT INTO DETALLE_FACTURAS(numero,descripcion,cantidad,valor,sub_total,estado,numero_factura)
VALUES(20,'Abono',2,400000,400000,true,01);



--11. angelica y su familia se retiran del Hotel y los datos quedan almacenados en la base de datos..

INSERT INTO PAGOS(numero, numero_cuenta, fecha_pago, hora_pago, valor, id_usuario, id_tipo_pago, nombre_paga) 
VALUES(01,01,'01/08/2021','13:00',850000,'hermoso',2,'Angelica Lizarazo')




















