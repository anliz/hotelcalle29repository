package capa_modelo;

import java.util.Date;

public class Reserva {

    private int codigo;
    private Date fecha;
    private Date fechaEntrada;
    private Date fechaSalida;
    private int cantidadPersonas;
    private boolean estado;
    private Cliente cliente;
    private Usuario usuario;
    private String tipoTarifa;

    public Reserva() {
    }

    public Reserva(int codigo, Date fecha, Date fechaEntrada, Date fechaSalida, int cantidadPersonas, boolean estado, Cliente cliente, Usuario usuario, String tipoTarifa) {
        this.codigo = codigo;
        this.fecha = fecha;
        this.fechaEntrada = fechaEntrada;
        this.fechaSalida = fechaSalida;
        this.cantidadPersonas = cantidadPersonas;
        this.estado = estado;
        this.cliente = cliente;
        this.usuario = usuario;
        this.tipoTarifa = tipoTarifa;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public Date getFechaEntrada() {
        return fechaEntrada;
    }

    public void setFechaEntrada(Date fechaEntrada) {
        this.fechaEntrada = fechaEntrada;
    }

    public Date getFechaSalida() {
        return fechaSalida;
    }

    public void setFechaSalida(Date fechaSalida) {
        this.fechaSalida = fechaSalida;
    }

    public int getCantidadPersonas() {
        return cantidadPersonas;
    }

    public void setCantidadPersonas(int cantidadPersonas) {
        this.cantidadPersonas = cantidadPersonas;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getTipoTarifa() {
        return tipoTarifa;
    }

    public void setTipoTarifa(String tipoTarifa) {
        this.tipoTarifa = tipoTarifa;
    }

    @Override
    public String toString() {
        return "Reserva{" + "codigo=" + codigo + ", fecha=" + fecha + ", fechaEntrada=" + fechaEntrada + ", fechaSalida=" + fechaSalida + ", cantidadPersonas=" + cantidadPersonas + ", estado=" + estado + ", cliente=" + cliente + ", usuario=" + usuario + ", tipoTarifa=" + tipoTarifa + '}';
    }

}
