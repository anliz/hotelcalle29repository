package capa_modelo;

import java.util.Date;

public class Factura {

    private int numero;
    private Date fecha;
    private double total;
    private boolean estado;
    private String observaciones;
    private Cuenta cuenta;
    private TipoPago tipoPago;
    private Cliente cliente;

    public Factura() {
    }

    public Factura(int numero, Date fecha, double total, boolean estado, String observaciones, Cuenta cuenta, TipoPago tipoPago, Cliente cliente) {
        this.numero = numero;
        this.fecha = fecha;
        this.total = total;
        this.estado = estado;
        this.observaciones = observaciones;
        this.cuenta = cuenta;
        this.tipoPago = tipoPago;
        this.cliente = cliente;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public TipoPago getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(TipoPago tipoPago) {
        this.tipoPago = tipoPago;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public String toString() {
        return "Factura{" + "numero=" + numero + ", fecha=" + fecha + ", total=" + total + ", estado=" + estado + ", observaciones=" + observaciones + ", cuenta=" + cuenta + ", tipoPago=" + tipoPago + ", cliente=" + cliente + '}';
    }

}
