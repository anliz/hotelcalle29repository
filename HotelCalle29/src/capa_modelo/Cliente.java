package capa_modelo;

import java.util.Date;

public class Cliente extends Persona {

    private String lugarExpedicion;
    private String ciudad;
    private String pais;
    private String ocupacion;
    private boolean estado;

    public Cliente() {
    }

    public Cliente(String lugarExpedicion, String ciudad, String pais, String ocupacion, boolean estado, int documento, String nombre, String apellido, Date fechaNacimiento, String direccion, String telefono, String email) {
        super(documento, nombre, apellido, fechaNacimiento, direccion, telefono, email);
        this.lugarExpedicion = lugarExpedicion;
        this.ciudad = ciudad;
        this.pais = pais;
        this.ocupacion = ocupacion;
        this.estado = estado;
    }

    

    public String getLugarExpedicion() {
        return this.lugarExpedicion;
    }

    public void setLugarExpedicion(String lugarExpedicion) {
        this.lugarExpedicion = lugarExpedicion;
    }

    public String getCiudad() {
        return this.ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    public String getPais() {
        return this.pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getOcupacion() {
        return this.ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public boolean getEstado() {
        return this.estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Cliente{" + "lugarExpedicion=" + lugarExpedicion + ", ciudad=" + ciudad + ", pais=" + pais + ", ocupacion=" + ocupacion + ", estado=" + estado + '}';
    }
}
