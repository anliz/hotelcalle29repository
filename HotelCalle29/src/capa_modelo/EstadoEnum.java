/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capa_modelo;

/**
 *
 * @author USER
 */
public enum EstadoEnum {
    
    ACTIVO(true), INACTIVO(false);
    
    private boolean estado;
    
    EstadoEnum(boolean estado){
        this.estado = estado;
    }

    public boolean getEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    
    
}
