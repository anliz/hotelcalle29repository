package capa_modelo;

public class ReservaHabitacion {

    private int id;
    private boolean estado;
    private Habitacion habitacion;
    private Reserva reserva;

    public ReservaHabitacion(int id, boolean estado, Habitacion habitacion, Reserva reserva) {
        this.id = id;
        this.estado = estado;
        this.habitacion = habitacion;
        this.reserva = reserva;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Habitacion getHabitacion() {
        return habitacion;
    }

    public void setHabitacion(Habitacion habitacion) {
        this.habitacion = habitacion;
    }

    public Reserva getReserva() {
        return reserva;
    }

    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }

    @Override
    public String toString() {
        return "ReservaHabitacion{" + "id=" + id + ", estado=" + estado + ", habitacion=" + habitacion + ", reserva=" + reserva + '}';
    }

}
