package capa_modelo;

public class TipoHabitacion {

    private int codigo;
    private String descripcion;
    private double tarifa1;
    private double tarifa2;
    private double tarifa3;
    private double adicional;
    private boolean estado;

    public TipoHabitacion() {

    }

    public TipoHabitacion(int codigo, String descripcion, double tarifa1, double tarifa2, double tarifa3, double adicional, boolean estado) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.tarifa1 = tarifa1;
        this.tarifa2 = tarifa2;
        this.tarifa3 = tarifa3;
        this.adicional = adicional;
        this.estado = estado;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getTarifa1() {
        return this.tarifa1;
    }

    public void setTarifa1(double tarifa1) {
        this.tarifa1 = tarifa1;
    }

    public double getTarifa2() {
        return this.tarifa2;
    }

    public void setTarifa2(double tarifa2) {
        this.tarifa2 = tarifa2;
    }

    public double getTarifa3() {
        return this.tarifa3;
    }

    public void setTarifa3(double tarifa3) {
        this.tarifa3 = tarifa3;
    }

    public double getAdicional() {
        return this.adicional;
    }

    public void setAdicional(double adicional) {
        this.adicional = adicional;
    }

    public boolean isEstado() {
        return this.estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "TipoHabitacion{" + "codigo=" + codigo + ", descripcion=" + descripcion + ", tarifa1=" + tarifa1 + ", tarifa2=" + tarifa2 + ", tarifa3=" + tarifa3 + ", adicional=" + adicional + ", estado=" + estado + '}';
    }
    
}
