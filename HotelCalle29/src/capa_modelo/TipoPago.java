package capa_modelo;

public class TipoPago {

    private int id;
    private String descripcion;
    private boolean estado;
    
    public TipoPago(){
        
    }
    
    public TipoPago(int id, String descripcion, boolean estado) {
        this.id = id;
        this.descripcion = descripcion;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "TipoPago{" + "id=" + id + ", descripcion=" + descripcion + ", estado=" + estado + '}';
    }

}
