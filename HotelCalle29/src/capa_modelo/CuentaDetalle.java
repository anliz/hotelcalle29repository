package capa_modelo;

import java.util.Date;

public class CuentaDetalle {

    private int numero;
    private String descripcion;
    private int cantidad;
    private double valor;
    private Date fecha;
    private boolean estado;
    private Cuenta cuenta;

    public CuentaDetalle(int numero, String descripcion, int cantidad, double valor, Date fecha, boolean estado, Cuenta cuenta) {
        this.numero = numero;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.valor = valor;
        this.fecha = fecha;
        this.estado = estado;
        this.cuenta = cuenta;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    @Override
    public String toString() {
        return "CuentaDetalle{" + "numero=" + numero + ", descripcion=" + descripcion + ", cantidad=" + cantidad + ", valor=" + valor + ", fecha=" + fecha + ", estado=" + estado + ", cuenta=" + cuenta + '}';
    }

}
