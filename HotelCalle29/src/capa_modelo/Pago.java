package capa_modelo;

import java.util.Date;

public class Pago {

    private int numero;
    private Cuenta cuenta;
    private Date fechaPago;
    private String horaPago;
    private String valor;
    private Usuario usuario;
    private TipoPago tipoPago;
    private String nombrePaga;

    public Pago(int numero, Cuenta cuenta, Date fechaPago, String horaPago, String valor, Usuario usuario, TipoPago tipoPago, String nombrePaga) {
        this.numero = numero;
        this.cuenta = cuenta;
        this.fechaPago = fechaPago;
        this.horaPago = horaPago;
        this.valor = valor;
        this.usuario = usuario;
        this.tipoPago = tipoPago;
        this.nombrePaga = nombrePaga;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public Date getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Date fechaPago) {
        this.fechaPago = fechaPago;
    }

    public String getHoraPago() {
        return horaPago;
    }

    public void setHoraPago(String horaPago) {
        this.horaPago = horaPago;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public TipoPago getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(TipoPago tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getNombrePaga() {
        return nombrePaga;
    }

    public void setNombrePaga(String nombrePaga) {
        this.nombrePaga = nombrePaga;
    }

    @Override
    public String toString() {
        return "Pago{" + "numero=" + numero + ", cuenta=" + cuenta + ", fechaPago=" + fechaPago + ", horaPago=" + horaPago + ", valor=" + valor + ", usuario=" + usuario + ", tipoPago=" + tipoPago + ", nombrePaga=" + nombrePaga + '}';
    }

}
