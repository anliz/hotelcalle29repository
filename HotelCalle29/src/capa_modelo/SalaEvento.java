package capa_modelo;

public class SalaEvento {

    private int numero;
    private int numeroPlanta;
    private String descripcion;
    private double tarifaHora;
    private boolean estado;

    public SalaEvento() {

    }

    public SalaEvento(int numero, int numeroPlanta, String descripcion, double tarifaHora, boolean estado) {
        this.numero = numero;
        this.numeroPlanta = numeroPlanta;
        this.descripcion = descripcion;
        this.tarifaHora = tarifaHora;
        this.estado = estado;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getNumeroPlanta() {
        return numeroPlanta;
    }

    public void setNumeroPlanta(int numeroPlanta) {
        this.numeroPlanta = numeroPlanta;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getTarifaHora() {
        return tarifaHora;
    }

    public void setTarifaHora(double tarifaHora) {
        this.tarifaHora = tarifaHora;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "SalaEvento{" + "numero=" + numero + ", numeroPlanta=" + numeroPlanta + ", descripcion=" + descripcion + ", tarifaHora=" + tarifaHora + ", estado=" + estado + '}';
    }

}
