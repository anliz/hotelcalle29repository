package capa_modelo;

import java.util.Date;

public class Huesped extends Persona {

    private int documento;
    private boolean estado;

    public Huesped(int documento, boolean estado) {
        this.documento = documento;
        this.estado = estado;
    }

    public Huesped(int documento, boolean estado, String nombre, String apellido, Date fechaNacimiento, String direccion, String telefono, String email) {
        super(documento, nombre, apellido, fechaNacimiento, direccion, telefono, email);
        this.documento = documento;
        this.estado = estado;
    }

    public int getDocumento() {
        return documento;
    }

    public void setDocumento(int documento) {
        this.documento = documento;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Huespede{" + "documento=" + documento + ", estado=" + estado + '}';
    }

}
