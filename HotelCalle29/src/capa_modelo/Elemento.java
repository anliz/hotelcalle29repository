package capa_modelo;

public class Elemento {

    private int codigo;
    private String descripcion;
    private int cantidad;
    private double valorAlquiler;
    private int disponible;
    private boolean estado;

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getValorAlquiler() {
        return valorAlquiler;
    }

    public void setValorAlquiler(double valorAlquiler) {
        this.valorAlquiler = valorAlquiler;
    }

    public int getDisponible() {
        return disponible;
    }

    public void setDisponible(int disponible) {
        this.disponible = disponible;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Elemento(int codigo, String descripcion, int cantidad, int disponible, boolean estado) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.disponible = disponible;
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Elemento{" + "codigo=" + codigo + ", descripcion=" + descripcion + ", cantidad=" + cantidad + ", valorAlquiler=" + valorAlquiler + ", disponible=" + disponible + ", estado=" + estado + '}';
    }

}
