package capa_modelo;

public class Habitacion {

    private int numero;
    private int numeroPlanta;
    private int numeroCamaSencilla;
    private int numeroCamaDoble;
    private String estadoOcupacion;
    private boolean estado;
    private TipoHabitacion tipoHabitacion;

    public Habitacion() {
    }

    public Habitacion(int numero, int numeroPlanta, int numeroCamaSencilla, int numeroCamaDoble, String estadoOcupacion, boolean estado, TipoHabitacion tipoHabitacion) {
        this.numero = numero;
        this.numeroPlanta = numeroPlanta;
        this.numeroCamaSencilla = numeroCamaSencilla;
        this.numeroCamaDoble = numeroCamaDoble;
        this.estadoOcupacion = estadoOcupacion;
        this.estado = estado;
        this.tipoHabitacion = tipoHabitacion;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getNumeroPlanta() {
        return numeroPlanta;
    }

    public void setNumeroPlanta(int numeroPlanta) {
        this.numeroPlanta = numeroPlanta;
    }

    public int getNumeroCamaSencilla() {
        return numeroCamaSencilla;
    }

    public void setNumeroCamaSencilla(int numeroCamaSencilla) {
        this.numeroCamaSencilla = numeroCamaSencilla;
    }

    public int getNumeroCamaDoble() {
        return numeroCamaDoble;
    }

    public void setNumeroCamaDoble(int numeroCamaDoble) {
        this.numeroCamaDoble = numeroCamaDoble;
    }

    public String getEstadoOcupacion() {
        return estadoOcupacion;
    }

    public void setEstadoOcupacion(String estadoOcupacion) {
        this.estadoOcupacion = estadoOcupacion;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public TipoHabitacion getTipoHabitacion() {
        return tipoHabitacion;
    }

    public void setTipoHabitacion(TipoHabitacion tipoHabitacion) {
        this.tipoHabitacion = tipoHabitacion;
    }

    @Override
    public String toString() {
        return "Habitacion{" + "numero=" + numero + ", numeroPlanta=" + numeroPlanta + ", numeroCamaSencilla=" + numeroCamaSencilla + ", numeroCamaDoble=" + numeroCamaDoble + ", estadoOcupacion=" + estadoOcupacion + ", estado=" + estado + ", tipoHabitacion=" + tipoHabitacion + '}';
    }

}
