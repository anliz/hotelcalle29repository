package capa_modelo;

public class Usuario {

    private String id;    
    private String contrasena;
    private Empleado empleado;

    public Usuario() {

    }

    public Usuario(String id, String contrasena, Empleado empleado) {
        this.id = id;
        this.contrasena = contrasena;
        this.empleado = empleado;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Empleado getEmpleado() {
        return empleado;
    }

    public void setEmpleado(Empleado empleado) {
        this.empleado = empleado;
    }

    @Override
    public String toString() {
        return "Usuario{" + "id=" + id + ", contrasena=" + contrasena + ", empleado=" + empleado + '}';
    }
}
