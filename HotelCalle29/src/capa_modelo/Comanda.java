package capa_modelo;

import java.util.Date;

public class Comanda {

    private int codigo;
    private Date fecha;
    private String horaComanda;
    private String descripcion;
    private double valor;
    private boolean estado;
    private Cuenta cuenta;
    private ServicioAdicional servicioAdicional;
    private Usuario usuario;

    public Comanda() {
    }

    public Comanda(int codigo, Date fecha, String horaComanda, String descripcion, double valor, boolean estado,
            Cuenta cuenta, ServicioAdicional servicioAdicional, Usuario usuario) {
        this.codigo = codigo;
        this.fecha = fecha;
        this.horaComanda = horaComanda;
        this.descripcion = descripcion;
        this.valor = valor;
        this.estado = estado;
        this.cuenta = cuenta;
        this.servicioAdicional = servicioAdicional;
        this.usuario = usuario;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public Date getFecha() {
        return this.fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getHoraComanda() {
        return this.horaComanda;
    }

    public void setHoraComanda(String horaComanda) {
        this.horaComanda = horaComanda;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getValor() {
        return this.valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public boolean isEstado() {
        return this.estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public ServicioAdicional getServicioAdicional() {
        return servicioAdicional;
    }

    public void setServicioAdicional(ServicioAdicional servicioAdicional) {
        this.servicioAdicional = servicioAdicional;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    @Override
    public String toString() {
        return "Comanda{" + "codigo=" + codigo + ", fecha=" + fecha + ", horaComanda=" + horaComanda + ", descripcion=" + descripcion + ", valor=" + valor + ", estado=" + estado + ", cuenta=" + cuenta + ", servicioAdicional=" + servicioAdicional + ", usuario=" + usuario + '}';
    }

}
