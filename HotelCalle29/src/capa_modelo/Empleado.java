package capa_modelo;

import java.util.Date;

public class Empleado extends Persona {

    private String cargo;
    private boolean estado;

    public Empleado() {

    }

    public Empleado(String cargo, boolean estado) {
        this.cargo = cargo;
        this.estado = estado;
    }

    public Empleado(String cargo, boolean estado, int documento, String nombre, String apellido, Date fechaNacimiento, String direccion, String telefono, String email) {
        super(documento, nombre, apellido, fechaNacimiento, direccion, telefono, email);
        this.cargo = cargo;
        this.estado = estado;
    }

   
    public String getCargo() {
        return this.cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public boolean getEstado() {
        return this.estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

}
