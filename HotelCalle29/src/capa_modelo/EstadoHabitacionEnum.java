/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capa_modelo;

/**
 *
 * @author USER
 */
public enum EstadoHabitacionEnum {
    
    DIPONIBLE("DISPONIBLE"), OCUPADA("OCUPADA"), RESERVADA("RESERVADA"), FUERA_DE_SERVICIO("FUERA DE SERVICIO");
    
    private String estado;
    
    EstadoHabitacionEnum(String estado){
        this.estado = estado;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    
    
    
    
}
