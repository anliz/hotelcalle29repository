package capa_modelo;

public class ServicioAdicional {
    
    private int codigo;
    private String descripcion;
    private double valor;
    private boolean estado;
    
    public ServicioAdicional(){
        
    }

    public ServicioAdicional(int codigo, String descripcion, double valor, boolean estado) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.valor = valor;
        this.estado = estado;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getValor() {
        return this.valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public boolean isEstado() {
        return this.estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }
    
    
}
