package capa_modelo;

import java.util.Date;

public class Cuenta {

    private int numero;
    private Date fechaApertura;
    private Date fechaCierre;
    private int cantidadPersonas;
    private int cantidadDias;
    private String tipoTarifa;
    private String observaciones;
    private double abono;
    private String estadoCuenta;
    private boolean estado;
    private Usuario usuario;
    private Cliente cliente;
    private Reserva reserva;

    public Cuenta() {

    }

    public Cuenta(int numero, Date fechaApertura, Date fechaCierre, int cantidadPersonas, int cantidadDias, String tipoTarifa, String observaciones, double abono, Usuario usuario, Cliente cliente, Reserva reserva) {
        this.numero = numero;
        this.fechaApertura = fechaApertura;
        this.fechaCierre = fechaCierre;
        this.cantidadPersonas = cantidadPersonas;
        this.cantidadDias = cantidadDias;
        this.tipoTarifa = tipoTarifa;
        this.observaciones = observaciones;
        this.abono = abono;
        this.usuario = usuario;
        this.cliente = cliente;
        this.reserva = reserva;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Date getFechaApertura() {
        return fechaApertura;
    }

    public void setFechaApertura(Date fechaApertura) {
        this.fechaApertura = fechaApertura;
    }

    public Date getFechaCierre() {
        return fechaCierre;
    }

    public void setFechaCierre(Date fechaCierre) {
        this.fechaCierre = fechaCierre;
    }

    public int getCantidadPersonas() {
        return cantidadPersonas;
    }

    public void setCantidadPersonas(int cantidadPersonas) {
        this.cantidadPersonas = cantidadPersonas;
    }

    public int getCantidadDias() {
        return cantidadDias;
    }

    public void setCantidadDias(int cantidadDias) {
        this.cantidadDias = cantidadDias;
    }

    public String getTipoTarifa() {
        return tipoTarifa;
    }

    public void setTipoTarifa(String tipoTarifa) {
        this.tipoTarifa = tipoTarifa;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public double getAbono() {
        return abono;
    }

    public void setAbono(double abono) {
        this.abono = abono;
    }

    public String getEstadoCuenta() {
        return estadoCuenta;
    }

    public void setEstadoCuenta(String estadoCuenta) {
        this.estadoCuenta = estadoCuenta;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Reserva getReserva() {
        return reserva;
    }

    public void setReserva(Reserva reserva) {
        this.reserva = reserva;
    }

    @Override
    public String toString() {
        return "Cuenta{" + "numero=" + numero + ", fechaApertura=" + fechaApertura + ", fechaCierre=" + fechaCierre + ", cantidadPersonas=" + cantidadPersonas + ", cantidadDias=" + cantidadDias + ", tipoTarifa=" + tipoTarifa + ", observaciones=" + observaciones + ", abono=" + abono + ", estadoCuenta=" + estadoCuenta + ", estado=" + estado + ", usuario=" + usuario + ", cliente=" + cliente + ", reserva=" + reserva + '}';
    }

    
}
