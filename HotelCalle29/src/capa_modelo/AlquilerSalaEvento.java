package capa_modelo;

import java.util.Date;

public class AlquilerSalaEvento {

    private int numero;
    private Date fecha;
    private String horaLlegada;
    private String horaSalida;
    private boolean confirmar;
    private boolean estado;
    private SalaEvento salaEvento;
    private Cuenta cuenta;
    private Cliente cliente;

    public AlquilerSalaEvento(int numero, Date fecha, String horaLlegada, String horaSalida, boolean confirmar, boolean estado,
            SalaEvento salaEvento, Cuenta cuenta, Cliente cliente) {

        this.numero = numero;
        this.fecha = fecha;
        this.horaLlegada = horaLlegada;
        this.horaSalida = horaSalida;
        this.confirmar = confirmar;
        this.estado = estado;
        this.salaEvento = salaEvento;
        this.cuenta = cuenta;
        this.cliente = cliente;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getHoraLlegada() {
        return horaLlegada;
    }

    public void setHoraLlegada(String horaLlegada) {
        this.horaLlegada = horaLlegada;
    }

    public String getHoraSalida() {
        return horaSalida;
    }

    public void setHoraSalida(String horaSalida) {
        this.horaSalida = horaSalida;
    }

    public boolean isConfirmar() {
        return confirmar;
    }

    public void setConfirmar(boolean confirmar) {
        this.confirmar = confirmar;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public SalaEvento getSalaEvento() {
        return salaEvento;
    }

    public void setSalaEvento(SalaEvento salaEvento) {
        this.salaEvento = salaEvento;
    }

    public Cuenta getCuenta() {
        return cuenta;
    }

    public void setCuenta(Cuenta cuenta) {
        this.cuenta = cuenta;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @Override
    public String toString() {
        return "AlquilerSalaEvento{" + "numero=" + numero + ", fecha=" + fecha + ", horaLlegada=" + horaLlegada + ", horaSalida=" + horaSalida + ", confirmar=" + confirmar + ", estado=" + estado + ", salaEvento=" + salaEvento + ", cuenta=" + cuenta + ", cliente=" + cliente + '}';
    }

}
