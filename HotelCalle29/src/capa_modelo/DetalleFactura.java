package capa_modelo;

public class DetalleFactura {

    private int numero;
    private String descripcion;
    private int cantidad;
    private double valor;
    private double subTotal;
    private boolean estado;
    private Factura factura;

    public DetalleFactura(int numero, String descripcion, int cantidad, double valor, double subTotal, boolean estado, Factura factura) {
        this.numero = numero;
        this.descripcion = descripcion;
        this.cantidad = cantidad;
        this.valor = valor;
        this.subTotal = subTotal;
        this.estado = estado;
        this.factura = factura;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(double subTotal) {
        this.subTotal = subTotal;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    public Factura getFactura() {
        return factura;
    }

    public void setFactura(Factura factura) {
        this.factura = factura;
    }

    @Override
    public String toString() {
        return "DetalleFactura{" + "numero=" + numero + ", descripcion=" + descripcion + ", cantidad=" + cantidad + ", valor=" + valor + ", subTotal=" + subTotal + ", estado=" + estado + ", factura=" + factura + '}';
    }

}
