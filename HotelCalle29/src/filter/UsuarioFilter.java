package filter;

public class UsuarioFilter {
    
    private String id;    
    private String contrasena;
    private Integer documentoEmpleado;

    public UsuarioFilter() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public Integer getDocumentoEmpleado() {
        return documentoEmpleado;
    }

    public void setDocumentoEmpleado(Integer documentoEmpleado) {
        this.documentoEmpleado = documentoEmpleado;
    }
    
    
    
}
