package filter;

import capa_modelo.*;

public class HabitacionFilter {

    private Integer numero;
    private Integer numeroPlanta;
    private Integer numeroCamaSencilla;
    private Integer numeroCamaDoble;
    private String estadoOcupacion;
    private Boolean estado;
    private Integer codigoTipoHabitacion;

    public HabitacionFilter() {
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public Integer getNumeroPlanta() {
        return numeroPlanta;
    }

    public void setNumeroPlanta(Integer numeroPlanta) {
        this.numeroPlanta = numeroPlanta;
    }

    public Integer getNumeroCamaSencilla() {
        return numeroCamaSencilla;
    }

    public void setNumeroCamaSencilla(Integer numeroCamaSencilla) {
        this.numeroCamaSencilla = numeroCamaSencilla;
    }

    public Integer getNumeroCamaDoble() {
        return numeroCamaDoble;
    }

    public void setNumeroCamaDoble(Integer numeroCamaDoble) {
        this.numeroCamaDoble = numeroCamaDoble;
    }

    public String getEstadoOcupacion() {
        return estadoOcupacion;
    }

    public void setEstadoOcupacion(String estadoOcupacion) {
        this.estadoOcupacion = estadoOcupacion;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }

    public Integer getCodigoTipoHabitacion() {
        return codigoTipoHabitacion;
    }

    public void setCodigoTipoHabitacion(Integer codigoTipoHabitacion) {
        this.codigoTipoHabitacion = codigoTipoHabitacion;
    }

}
