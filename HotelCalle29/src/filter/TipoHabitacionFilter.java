package filter;

public class TipoHabitacionFilter {
    private Integer codigo;
    private String descripcion;
    private Double tarifa1;
    private Double tarifa2;
    private Double tarifa3;
    private Double adicional;
    private Boolean estado;

    public TipoHabitacionFilter() {
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Double getTarifa1() {
        return tarifa1;
    }

    public void setTarifa1(Double tarifa1) {
        this.tarifa1 = tarifa1;
    }

    public Double getTarifa2() {
        return tarifa2;
    }

    public void setTarifa2(Double tarifa2) {
        this.tarifa2 = tarifa2;
    }

    public Double getTarifa3() {
        return tarifa3;
    }

    public void setTarifa3(Double tarifa3) {
        this.tarifa3 = tarifa3;
    }

    public Double getAdicional() {
        return adicional;
    }

    public void setAdicional(Double adicional) {
        this.adicional = adicional;
    }

    public Boolean getEstado() {
        return estado;
    }

    public void setEstado(Boolean estado) {
        this.estado = estado;
    }
    
    
    
}
