package capa_negocio;

import conexion.Conexion;
import capa_modelo.Cliente;
import capa_modelo.EstadoEnum;
import capa_servicios.ClienteServiceImpl;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public class ManejoCliente {
    
    private static ClienteServiceImpl clienteService;
    
    public static void main(String[] args) {
        // Aplicando transacciones debe realizar las siguientes tareas:
        
        Connection conexion = null;
        try {
            conexion = Conexion.getConnection();
            if(conexion.getAutoCommit()){
                conexion.setAutoCommit(false);
            }
            // Listar todas los clientes
            clienteService = new ClienteServiceImpl();
            List<Cliente> clientes = clienteService.listAll();
            for(Cliente cliente : clientes){
                System.out.println("Clientes: " + clientes );
            }
            // Insertar un cliente de ejemplo
            Cliente nuevoCliente = new Cliente();

            nuevoCliente.setDocumento(56123);
            nuevoCliente.setLugarExpedicion("BOGOTA");
            nuevoCliente.setCiudad("BOGOTA");
            nuevoCliente.setPais("COLOMBIA");
            nuevoCliente.setOcupacion("ADMINISTRADOR");
            nuevoCliente.setEstado(EstadoEnum.ACTIVO.getEstado());
            nuevoCliente.setNombre("ENRIQUE");
            nuevoCliente.setApellido("GAMBOA LIZCANO");
            nuevoCliente.setFechaNacimiento(Utils.getDateFromString("1994/12/12"));
            nuevoCliente.setDireccion("CALLE 14 N° 28-23");
            nuevoCliente.setTelefono("6666666");
            nuevoCliente.setEmail("ENRIQUEG@GMAIL.COM");
            clienteService.insert(nuevoCliente);
            
            // actualizar un cliente de ejemplo
            Cliente cambiarCliente = new Cliente();

            cambiarCliente.setDocumento(1109238);
            cambiarCliente.setLugarExpedicion("SALAZAR");
            cambiarCliente.setCiudad("CUCUTA");
            cambiarCliente.setPais("COLOMBIA");
            cambiarCliente.setOcupacion("INGENIERO CIVIL");
            cambiarCliente.setEstado(EstadoEnum.ACTIVO.getEstado());
            cambiarCliente.setNombre("PEDRO ENRIQUE");
            cambiarCliente.setApellido("MORA SUAREZ");
            cambiarCliente.setFechaNacimiento (Utils.getDateFromString("1990/07/12"));
            cambiarCliente.setDireccion("ANILLO VIAL ");
            cambiarCliente.setTelefono("3213450987");
            cambiarCliente.setEmail("PEDROG@gmail.com");
            
            clienteService.update(cambiarCliente);
            
            // eliminar un cliente de ejemplo
            Cliente eliminarCliente = new Cliente();
            eliminarCliente.setDocumento(9876);
            clienteService.delete(eliminarCliente);
            
            conexion.commit();
        } catch (SQLException ex) {
             ex.printStackTrace(System.out);
             System.err.println("Entramos al Rollback");
            try {
                conexion.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace(System.out);
            }
            
        } catch (ParseException pe){
            System.err.println("Errro parsing date:" + pe.getMessage());
        }
    }
    
}
