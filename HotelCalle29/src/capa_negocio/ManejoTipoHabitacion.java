package capa_negocio;

import conexion.Conexion;
import capa_modelo.EstadoEnum;
import capa_modelo.TipoHabitacion;
import capa_servicios.TipoHabitacionServiceImpl;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;


public class ManejoTipoHabitacion {
        
    private static TipoHabitacionServiceImpl tipoHabitacionService;
    
    public static void main(String[] args) {
         // Aplicando transacciones debe realizar las siguientes tareas:
        
        Connection conexion = null;
        try {
            conexion = Conexion.getConnection();
            if(conexion.getAutoCommit()){
                conexion.setAutoCommit(false);
            }
            // Listar todas las Tipo Habitaciones
            tipoHabitacionService = new TipoHabitacionServiceImpl();
            List<TipoHabitacion> tipoHabitaciones = tipoHabitacionService.listAll();
            for(TipoHabitacion tipoHabitacion : tipoHabitaciones){
                System.out.println("Tipo Habitaciones: " + tipoHabitaciones );
            }
            // Insertar un TipHabitacion de ejemplo
            TipoHabitacion nuevoTipoHabitacion = new TipoHabitacion();

            nuevoTipoHabitacion.setCodigo(5);
            nuevoTipoHabitacion.setDescripcion("SUITE");
            //nuevoTipoHabitacion.setTarifa1();
            //nuevoTipoHabitacion.setTarifa2();
            nuevoTipoHabitacion.setTarifa3(100000.00);   
            nuevoTipoHabitacion.setAdicional(30000.00);
            nuevoTipoHabitacion.setEstado(EstadoEnum.ACTIVO.getEstado());
            tipoHabitacionService.insert(nuevoTipoHabitacion);
            
            // actualizar un tipo habitacion de ejemplo
            TipoHabitacion cambiarTipoHabitacion = new TipoHabitacion();
            
            cambiarTipoHabitacion.setCodigo(2);
            cambiarTipoHabitacion.setDescripcion("SUITE");
            cambiarTipoHabitacion.setTarifa1(200000);
            //cambiarTipoHabitacion.setTarifa2();
            //cambiarTipoHabitacion.setTarifa3();   
            cambiarTipoHabitacion.setAdicional(20000);
            cambiarTipoHabitacion.setEstado(EstadoEnum.ACTIVO.getEstado());
            tipoHabitacionService.update(cambiarTipoHabitacion);

            
            // eliminar un tipo habitación de ejemplo
             TipoHabitacion eliminarTipoHabitacion = new TipoHabitacion();
             eliminarTipoHabitacion.setCodigo(4);
             tipoHabitacionService.delete(eliminarTipoHabitacion);
            
            conexion.commit();
        } catch (SQLException ex) {
             ex.printStackTrace(System.out);
             System.out.println("Entramos al Rollback");
            try {
                conexion.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace(System.out);
            }
            
        }
    }
    
}
