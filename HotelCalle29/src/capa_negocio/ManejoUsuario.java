package capa_negocio;

import conexion.Conexion;
import capa_modelo.Empleado;
import capa_modelo.Usuario;
import capa_servicios.UsuarioServiceImpl;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class ManejoUsuario {
    
    private static UsuarioServiceImpl usuarioService;
    
    public static void main(String[] args) {
        // Aplicando transacciones debe realizar las siguientes tareas:
        Connection conexion = null;
        try {
            conexion = Conexion.getConnection();
            if(conexion.getAutoCommit()){
                conexion.setAutoCommit(false);
            }
            // Listar todas las Habitaciones
             usuarioService = new UsuarioServiceImpl();
            List<Usuario> usuarios = usuarioService.listAll();
            for(Usuario usuario : usuarios){
                System.out.println("Usuarios: " + usuarios );
            }
            // Insertar un usuario de ejemplo
            Usuario nuevoUsuario = new Usuario();
            Empleado e = new Empleado();
            
            nuevoUsuario.setId("mariap");
            nuevoUsuario.setContrasena("0987");
            nuevoUsuario.setEmpleado(e);
            e.setDocumento(1090353361);
            usuarioService.insert(nuevoUsuario);
            
            // actualizar un usuario de ejemplo
            Usuario cambiarUsuario = new Usuario();
  
            cambiarUsuario.setId("angelicaL");
            cambiarUsuario.setContrasena("3344");
            cambiarUsuario.setEmpleado(e);
            e.setDocumento(1094353457);
            usuarioService.update(cambiarUsuario);
            
            // eliminar un usuario de ejemplo
             Usuario eliminarUsuario = new Usuario();
             eliminarUsuario.setId("SofiaS");
             usuarioService.delete(eliminarUsuario);
            
            conexion.commit();
        } catch (SQLException ex) {
             ex.printStackTrace(System.out);
             System.out.println("Entramos al Rollback");
            try {
                conexion.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace(System.out);
            }
            
        }
    }
}
