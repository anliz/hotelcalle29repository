package capa_negocio;

import capa_modelo.Cliente;
import capa_modelo.EstadoEnum;
import capa_modelo.Reserva;
import capa_modelo.Usuario;
import capa_servicios.ReservaServiceImpl;
import conexion.Conexion;
import filter.ReservaFilter;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;


public class ManejoReserva {
    
     private static ReservaServiceImpl reservaService;
    
    public static void main(String[] args) {
        
        // Aplicando transacciones debe realizar las siguientes tareas:
        
       Connection conexion = null;
        try {
            conexion = Conexion.getConnection();
            if(conexion.getAutoCommit()){
                conexion.setAutoCommit(false);
            }

             reservaService = new ReservaServiceImpl();

            // Listar todas las Reservas
            List<Reserva> reservas = reservaService.listAll();
            /*for(Reservas reserva : reservas) {
                System.out.println("Reservas: " + reserva.toString());
            }
            */
            ReservaFilter filtro = new ReservaFilter();
            filtro.setCantidadPersonas(2);
            filtro.setTipoTarifa("ESTANDAR");
            List<Reserva> reservasFiltradas = reservaService.filter(filtro);
            for(Reserva reserva : reservasFiltradas) {
                System.out.println("Reserva: " + reserva.toString());
            }
            
            /**
             * 
             * Realizar la misma separación de capas.. con los demás métodos:
             * insertar
             * update
             * delete
             * getHabitacion
             * filter
             */
           
            // Insertar una reserva de ejemplo
            Reserva nuevaReserva = new Reserva();
            Usuario usuario = new Usuario();
            Cliente cliente = new Cliente();
            
            nuevaReserva.setCodigo(4);
            nuevaReserva.setFecha(Utils.getDateFromString("2020/12/12"));
            nuevaReserva.setFechaEntrada(Utils.getDateFromString("2020/12/15"));
            nuevaReserva.setFechaSalida(Utils.getDateFromString("2020/12/28"));
            nuevaReserva.setCantidadPersonas(2);
            nuevaReserva.setEstado(EstadoEnum.ACTIVO.getEstado());
            nuevaReserva.setCliente(cliente);
            cliente.setDocumento(1109238);
            nuevaReserva.setUsuario(usuario);
            usuario.setId("mariap");
            nuevaReserva.setTipoTarifa("estandar");
            
            reservaService.insert(nuevaReserva);
            
            // actualizar una reserva de ejemplo
            Reserva cambiarReserva = new Reserva();
  
            cambiarReserva.setCodigo(2);
            cambiarReserva.setFecha(Utils.getDateFromString("2020/05/03"));
            cambiarReserva.setFechaEntrada(Utils.getDateFromString("2020/05/10"));
            cambiarReserva.setFechaSalida(Utils.getDateFromString("2020/05/25"));
            cambiarReserva.setCantidadPersonas(2);
            cambiarReserva.setEstado(EstadoEnum.ACTIVO.getEstado());
            cambiarReserva.setCliente(cliente);
            cliente.setDocumento(56123);
            cambiarReserva.setUsuario(usuario);
            usuario.setId("mariap");
            cambiarReserva.setTipoTarifa("estandar");
            reservaService.update(cambiarReserva);
            
            // eliminar una reserva de ejemplo
             Reserva eliminarReserva = new Reserva();
             eliminarReserva.setCodigo(1);
             reservaService.delete(eliminarReserva);
            
            conexion.commit();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            System.out.println("Entramos al Rollback");

        } catch (ParseException ex) {
             System.err.println("Errro parsing date:" + ex.getMessage());
         }
    }
    
}
