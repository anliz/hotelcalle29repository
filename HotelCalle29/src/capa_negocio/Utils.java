/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capa_negocio;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author USER
 */
public class Utils {
    
    // public static final String FORMAT_DATE = "yyyy/MM/dd hh:mm:ss";
    public static final String FORMAT_DATE = "yyyy/MM/dd";
    
    public static Date getDateFromString(String fecha) throws ParseException{
        SimpleDateFormat spd = new SimpleDateFormat(FORMAT_DATE);
        Date fechaDate = spd.parse(fecha);
        return fechaDate;
    }
    
    public static String getStringFromDate (Date fecha){
        SimpleDateFormat spd = new SimpleDateFormat(FORMAT_DATE);
        String fechaString = spd.format(fecha);
        return fechaString;
    }
    
}
