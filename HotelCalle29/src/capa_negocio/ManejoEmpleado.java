package capa_negocio;

import conexion.Conexion;
import capa_modelo.Empleado;
import capa_modelo.EstadoEnum;
import capa_servicios.EmpleadoServiceImpl;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.List;

public class ManejoEmpleado {
    
    private static EmpleadoServiceImpl empleadoService;
    
    public static void main(String[] args) {

        // Aplicando transacciones debe realizar las siguientes tareas:
        
        Connection conexion = null;
        try {
            conexion = Conexion.getConnection();
            if(conexion.getAutoCommit()){
                conexion.setAutoCommit(false);
            }
            // Listar todas los empleados
            empleadoService = new EmpleadoServiceImpl();
            List<Empleado> empleados = empleadoService.listAll();
            for(Empleado empleado : empleados){
                System.out.println("Empleados: " + empleado.toString() );
            }
            // Insertar un empleado de ejemplo
            Empleado nuevoEmpleado = new Empleado();

            nuevoEmpleado.setDocumento(123456);
            nuevoEmpleado.setCargo("TECNICO");
            nuevoEmpleado.setEstado(EstadoEnum.ACTIVO.getEstado());
            nuevoEmpleado.setNombre("KARLO");
            nuevoEmpleado.setApellido("PEREZ GAMBOA");
            nuevoEmpleado.setFechaNacimiento(Utils.getDateFromString("1994/12/12"));
            nuevoEmpleado.setDireccion("CALLE 14 N° 28-23");
            nuevoEmpleado.setTelefono("6666666");
            nuevoEmpleado.setEmail("KARLA@GMAIL.COM");
            empleadoService.insert(nuevoEmpleado);
            
            // actualizar un empleado de ejemplo
            Empleado cambiarEmpleado = new Empleado();

            cambiarEmpleado.setDocumento(1090383368);
            cambiarEmpleado.setCargo("Administrador");
            cambiarEmpleado.setEstado(EstadoEnum.ACTIVO.getEstado());
            cambiarEmpleado.setNombre("Wilfred Uriel");
            cambiarEmpleado.setApellido("Garcia");
            cambiarEmpleado.setFechaNacimiento (Utils.getDateFromString("1990/07/12"));
            cambiarEmpleado.setDireccion("ANILLO VIAL ");
            cambiarEmpleado.setTelefono("3219893104");
            cambiarEmpleado.setEmail("Urielg12@gmail.com");
            
            empleadoService.update(cambiarEmpleado);
            
            // eliminar un empleado de ejemplo
            Empleado eliminarEmpleado = new Empleado();
             eliminarEmpleado.setDocumento(12345);
             empleadoService.delete(eliminarEmpleado);
            
            conexion.commit();
        } catch (SQLException ex) {
             ex.printStackTrace(System.out);
             System.err.println("Entramos al Rollback");
            try {
                conexion.rollback();
            } catch (SQLException ex1) {
                ex1.printStackTrace(System.out);
            }
            
        } catch (ParseException pe){
            System.err.println("Errro parsing date:" + pe.getMessage());
        }
    }
    
}
