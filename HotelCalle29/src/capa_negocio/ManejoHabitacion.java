package capa_negocio;

import capa_modelo.EstadoEnum;
import capa_modelo.EstadoHabitacionEnum;
import capa_modelo.Habitacion;
import capa_modelo.TipoHabitacion;
import capa_servicios.HabitacionServiceImpl;
import conexion.Conexion;
import filter.HabitacionFilter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class ManejoHabitacion {
    
    private static HabitacionServiceImpl habitacionService;

    public static void main(String args[]) {
        

        // Aplicando transacciones debe realizar las siguientes tareas:
        
       Connection conexion = null;
        try {
            conexion = Conexion.getConnection();
            if(conexion.getAutoCommit()){
                conexion.setAutoCommit(false);
            }

             habitacionService = new HabitacionServiceImpl();

            // Listar todas las Habitaciones
            List<Habitacion> habitaciones = habitacionService.listAll();
            /*for(Habitacion habitacion : habitaciones) {
                System.out.println("Habitación: " + habitacion.toString());
            }
            */
            HabitacionFilter filtro = new HabitacionFilter();
            filtro.setNumeroPlanta(1);
            filtro.setNumeroCamaSencilla(0);
            List<Habitacion> habitacionesFiltradas = habitacionService.filter(filtro);
            for(Habitacion habitacion : habitacionesFiltradas) {
                System.out.println("Habitación: " + habitacion.toString());
            }
            
            /**
             * 
             * Realizar la misma separación de capas.. con los demás métodos:
             * insertar
             * update
             * delete
             * getHabitacion
             * filter
             */
           
            // Insertar una habitacion de ejemplo
            Habitacion nuevaHabitacion = new Habitacion();
            TipoHabitacion th = new TipoHabitacion();
            
            nuevaHabitacion.setNumero(12);
            nuevaHabitacion.setNumeroPlanta(3);
            nuevaHabitacion.setNumeroCamaSencilla(1);
            nuevaHabitacion.setNumeroCamaDoble(2);
            nuevaHabitacion.setEstadoOcupacion(EstadoHabitacionEnum.OCUPADA.getEstado());
            nuevaHabitacion.setEstado(EstadoEnum.ACTIVO.getEstado());
            nuevaHabitacion.setTipoHabitacion(th);
            th.setCodigo(1);
            habitacionService.insert(nuevaHabitacion);
            
            // actualizar una habitación de ejemplo
            Habitacion cambiarHabitacion = new Habitacion();
  
            cambiarHabitacion.setNumero(13);
            cambiarHabitacion.setNumeroPlanta(2);
            cambiarHabitacion.setNumeroCamaSencilla(3);
            cambiarHabitacion.setNumeroCamaDoble(0);
            cambiarHabitacion.setEstadoOcupacion(EstadoHabitacionEnum.DIPONIBLE.getEstado());
            cambiarHabitacion.setEstado(EstadoEnum.ACTIVO.getEstado());
            cambiarHabitacion.setTipoHabitacion(th);
            th.setCodigo(2);
            habitacionService.update(cambiarHabitacion);
            
            // eliminar una habitación de ejemplo
             Habitacion eliminarHabitacion = new Habitacion();
             eliminarHabitacion.setNumero(15);
             habitacionService.delete(eliminarHabitacion);
            
            conexion.commit();

        } catch (SQLException ex) {
            ex.printStackTrace(System.out);
            System.out.println("Entramos al Rollback");

        }

    }

}
