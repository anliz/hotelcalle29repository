package capa_datos;


import capa_modelo.Empleado;
import filter.EmpleadoFilter;
import java.sql.SQLException;
import java.util.List;


public interface EmpleadoDao {
    
    List<Empleado> listAll() throws SQLException;
    
    Empleado getEmpleado(Integer documento) throws SQLException;
    
    Integer insert(Empleado empleado) throws SQLException;
    
    Integer update(Empleado empleado) throws SQLException;
    
    Integer delete(Empleado empleado) throws SQLException;
    
    List<Empleado> filter(EmpleadoFilter filter) throws SQLException;
    
}
