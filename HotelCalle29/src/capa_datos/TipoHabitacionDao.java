package capa_datos;

import capa_modelo.TipoHabitacion;
import filter.TipoHabitacionFilter;
import java.sql.SQLException;
import java.util.List;

public interface TipoHabitacionDao {
    
    
    List<TipoHabitacion> listAll() throws SQLException;
    
    TipoHabitacion getTipoHabitacion(Integer codigo) throws SQLException;
    
    Integer insert(TipoHabitacion tipoHabitacion) throws SQLException;
    
    Integer update(TipoHabitacion tipoHabitacion) throws SQLException;
    
    Integer delete(TipoHabitacion tipoHabitacion) throws SQLException;
    
    List<TipoHabitacion> filter(TipoHabitacionFilter filter) throws SQLException;
}
