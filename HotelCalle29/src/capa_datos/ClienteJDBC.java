package capa_datos;

import conexion.Conexion;
import capa_modelo.Cliente;
import capa_negocio.Utils;
import filter.ClienteFilter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ClienteJDBC implements ClienteDao{
    
    private Connection conexionTransaccional;

    private static final String SQL_LIST_ALL = "SELECT c.lugar_expedicion, c.ciudad, c.pais, c.ocupacion, c.estado, p.documento, p.nombre, p.apellido, p.fecha_nacimiento,"
            + " p.direccion, p.telefono, p.email FROM clientes as c, personas as p WHERE c.documento = p.documento";            

    private static final String SQL_SELECT_CLIENTE_POR_DOCUMENTO = "SELECT c.lugar_expedicion = ?, c.ciudad = ?, c.pais =?, c.ocupacion =?, c.estado = ?, p.documento = ?, p.nombre = ?, p.apellido = ?, p.fecha_nacimiento = ?,"
            + " p.direccion, p.telefono, p.email FROM clientes as e, personas as p WHERE e.documento = p.documento AND e.documento = ?";

    private static final String SQL_INSERT_CLIENTE = "INSERT INTO clientes (documento,lugar_expedicion,ciudad,pais,ocupacion,estado) VALUES (?,?,?,?,?,?);";
    
    private static final String SQL_INSERT_PERSONA = "INSERT INTO personas (documento,nombre,apellido,fecha_nacimiento,direccion,telefono,email) VALUES (?,?,?,date(?),?,?,?);";

    private static final String SQL_UPDATE_CLIENTE = "UPDATE clientes SET lugar_expedicion = ?, ciudad = ?, pais =?, ocupacion =? , estado =? WHERE documento = ?";
    
    private static final String SQL_UPDATE_PERSONA = "UPDATE personas SET  nombre =?, apellido =?, fecha_nacimiento = date(?), direccion =?, telefono =?, email =? WHERE documento = ?";

    private static final String SQL_DElETE_CLIENTE = "DELETE FROM clientes WHERE documento = ?";
    
    private static final String SQL_DElETE_PERSONA = "DELETE FROM personas WHERE documento = ?";

    public ClienteJDBC() {
    }

    public ClienteJDBC(Connection conexionTransaccional) {
        this.conexionTransaccional = conexionTransaccional;
    }

    @Override
    public List<Cliente> listAll() throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Cliente cliente = null;
        List<Cliente> clientes = new ArrayList<Cliente>();
        
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_LIST_ALL);
            rs = stmt.executeQuery();
            while (rs.next()) {
                String lugarExpedicion = rs.getString("lugar_expedicion");
                String ciudad = rs.getString("ciudad");
                String pais = rs.getString("pais");
                String ocupacion = rs.getString("ocupacion");
                boolean estado = rs.getBoolean("estado");
                int documento = rs.getInt("documento");
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                Date fechaNacimiento = rs.getDate("fecha_nacimiento");
                String direccion = rs.getString("direccion");
                String telefono = rs.getString("telefono");
                String email = rs.getString("email");
   
                cliente = new Cliente();
                
                cliente.setLugarExpedicion(lugarExpedicion);
                cliente.setCiudad(ciudad);
                cliente.setPais(pais);
                cliente.setOcupacion(ocupacion);
                cliente.setEstado(estado);
                cliente.setDocumento(documento);
                cliente.setNombre(nombre);
                cliente.setApellido(apellido);
                cliente.setFechaNacimiento(fechaNacimiento);
                cliente.setDireccion(direccion);
                cliente.setTelefono(telefono);
                cliente.setEmail(email);
                
            }
        } finally {
            
            Conexion.close(rs);
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return clientes;
    }
    
    @Override
    public Integer insert(Cliente cliente) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            
            this.insertPersona(cliente);
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT_CLIENTE);
            
            stmt.setInt(1, cliente.getDocumento());
            stmt.setString(2, cliente.getLugarExpedicion().toUpperCase());
            stmt.setString(3, cliente.getCiudad().toUpperCase());
            stmt.setString(4, cliente.getPais().toUpperCase());
            stmt.setString(5, cliente.getOcupacion().toUpperCase());
            stmt.setBoolean(6, cliente.getEstado());

            System.out.println("Ejecutando Query: " + SQL_INSERT_CLIENTE);
            rows = stmt.executeUpdate();
            System.out.println("Registros afectados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }
    private Integer insertPersona(Cliente cliente) throws SQLException{
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;
        
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT_PERSONA);
            stmt.setInt(1, cliente.getDocumento());
            stmt.setString(2, cliente.getNombre().toUpperCase());
            stmt.setString(3, cliente.getApellido().toUpperCase());
            stmt.setString(4, Utils.getStringFromDate(cliente.getFechaNacimiento()));
            stmt.setString(5, cliente.getDireccion().toUpperCase());
            stmt.setString(6, cliente.getTelefono());
            stmt.setString(7, cliente.getEmail().toUpperCase());
            
            System.out.println("Ejecutando Query: " + SQL_INSERT_PERSONA);
            rows = stmt.executeUpdate();
            System.out.println("Registros afectados: " + rows);
            
        }finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;    
    }
    @Override
    public Integer update(Cliente cliente) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            this.updatePersona(cliente);
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            System.out.println("Ejecutando Query: " + SQL_UPDATE_CLIENTE);
            stmt = conn.prepareStatement(SQL_UPDATE_CLIENTE);
 
            stmt.setString(1, cliente.getLugarExpedicion().toUpperCase());
            stmt.setString(2, cliente.getCiudad().toUpperCase());
            stmt.setString(3, cliente.getPais().toUpperCase());
            stmt.setString(4, cliente.getOcupacion().toUpperCase());
            stmt.setBoolean(5, cliente.getEstado());
            stmt.setInt(6, cliente.getDocumento());
            
            rows = stmt.executeUpdate();
            System.out.println("Registros actualizados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }
    private Integer updatePersona(Cliente cliente) throws SQLException{
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;
        
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
           
            System.out.println("Ejecutando Query: " + SQL_UPDATE_PERSONA);
            stmt = conn.prepareStatement(SQL_UPDATE_PERSONA);
      
            stmt.setString(1, cliente.getNombre().toUpperCase());
            stmt.setString(2, cliente.getApellido().toUpperCase());
            stmt.setString(3, Utils.getStringFromDate(cliente.getFechaNacimiento()));
            stmt.setString(4, cliente.getDireccion().toUpperCase());
            stmt.setString(5, cliente.getTelefono());
            stmt.setString(6, cliente.getEmail().toUpperCase());
            stmt.setInt(7, cliente.getDocumento());
            
            rows = stmt.executeUpdate();
            System.out.println("Registros actualizados: " + rows);
            
        }finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }

    @Override
    public Integer delete(Cliente cliente) throws SQLException {
         Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            System.out.println("Ejecutando Query: " + SQL_DElETE_CLIENTE);
            stmt = conn.prepareStatement(SQL_DElETE_CLIENTE);
            stmt.setInt(1, cliente.getDocumento());
            rows = stmt.executeUpdate();
            System.out.println("Registros eliminados: " + rows);
            this.deletePersona(cliente);
            
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }
   
    private Integer deletePersona(Cliente cliente) throws SQLException{
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;
         try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_DElETE_PERSONA);
            stmt.setInt(1, cliente.getDocumento());
            rows = stmt.executeUpdate();
            System.out.println("Registros eliminados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }
    
    @Override
    public Cliente getCliente(Integer documento) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Cliente cliente = null;

        conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
        stmt = conn.prepareStatement(SQL_SELECT_CLIENTE_POR_DOCUMENTO);
        stmt.setInt(1, documento);
        rs = stmt.executeQuery();

        if (rs.next()) {
            cliente = new Cliente();
            cliente.setDocumento(rs.getInt("documento"));
            cliente.setLugarExpedicion("lugar_expedicion");
            cliente.setCiudad(rs.getString("ciudad"));
            cliente.setPais(rs.getString("pais"));
            cliente.setOcupacion(rs.getString("ocupacion"));
            cliente.setEstado(rs.getBoolean("estado"));
            cliente.setNombre(rs.getString("nombre"));
            cliente.setApellido(rs.getString("apellido"));
            cliente.setFechaNacimiento(rs.getDate("fecha_nacimiento"));
            cliente.setDireccion(rs.getString("direccion"));
            cliente.setTelefono(rs.getString("telefono"));
            cliente.setEmail(rs.getString("email"));

           return (cliente);
        }
        return (null);
    }
     @Override
    public List<Cliente> filter(ClienteFilter filter) throws SQLException {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Cliente cliente = null;
        List<Cliente> clientes = new ArrayList<Cliente>();
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            String query = this.addFilters(SQL_LIST_ALL, filter);    
            System.out.println("Ejecutando Query: " + query);
            stmt = conn.prepareStatement(query);            
            while (rs.next()) {
                
                String lugarExpedicion = rs.getString("lugar_expedicion");
                String ciudad = rs.getString("ciudad");
                String pais = rs.getString("pais");
                String ocupacion = rs.getString("ocupacion");
                boolean estado = rs.getBoolean("estado");
                int documento = rs.getInt("documento");
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                Date fechaNacimiento = rs.getDate("fecha_nacimiento");
                String direccion = rs.getString("direccion");
                String telefono = rs.getString("telefono");
                String email = rs.getString("email");
   
                cliente = new Cliente();
                
                cliente.setLugarExpedicion(lugarExpedicion);
                cliente.setCiudad(ciudad);
                cliente.setPais(pais);
                cliente.setOcupacion(ocupacion);
                cliente.setEstado(estado);
                cliente.setDocumento(documento);
                cliente.setNombre(nombre);
                cliente.setApellido(apellido);
                cliente.setFechaNacimiento(fechaNacimiento);
                cliente.setDireccion(direccion);
                cliente.setTelefono(telefono);
                cliente.setEmail(email);
                
                clientes.add(cliente);
            }
        } finally {
            Conexion.close(rs);
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }

        return clientes;
        
    }

   private String addFilters(String query, ClienteFilter filter){
       if(filter == null)
           return query;
       
       String filtrosWhere = "";
       
       if(filter.getDocumento() != null){
           filtrosWhere += "documento = " + filter.getDocumento();
       }
       
       if(filter.getLugarExpedicion() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "lugar_expedicion = " + filter.getLugarExpedicion();
       }
       
       if(filter.getCiudad()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "ciudad = " + filter.getCiudad();
       }
       
       if(filter.getPais()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "pais = " + filter.getPais();
       }
       if(filter.getOcupacion()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "ocupacion = " + filter.getOcupacion();
       }

       if(filter.getEstado() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "estado = " + filter.getEstado();
       }
       
       if(filter.getNombre() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "nombre = " + filter.getNombre();
       }
       
       if(filter.getApellido() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "apellido = " + filter.getApellido();
       }
       
       if(filter.getFechaNacimiento() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "fecha_nacimiento = " + filter.getFechaNacimiento();
       }
       
       if(filter.getDireccion() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "direccion = " + filter.getDireccion();
       }
       
       if(filter.getTelefono()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "telefono = " + filter.getTelefono();
       }
       
       if(filter.getEmail()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "email = " + filter.getEmail();
       }
       
       if(!filtrosWhere.isEmpty()){
           query = query + " WHERE " + filtrosWhere;
       }
       
       return query;
   }

    
}
