package capa_datos;

import conexion.Conexion;
import capa_modelo.Empleado;
import capa_negocio.Utils;
import filter.EmpleadoFilter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class EmpleadoJDBC implements EmpleadoDao{
    
    private Connection conexionTransaccional;

    private static final String SQL_LIST_ALL = "SELECT e.cargo, e.estado, p.documento, p.nombre, p.apellido, p.fecha_nacimiento,"
            + " p.direccion, p.telefono, p.email FROM empleados as e, personas as p WHERE e.documento = p.documento";            

    private static final String SQL_SELECT_EMPLEADO_POR_DOCUMENTO = "SELECT e.cargo, e.estado, p.documento, p.nombre, p.apellido, p.fecha_nacimiento,"
            + " p.direccion, p.telefono, p.email FROM empleados as e, personas as p WHERE e.documento = p.documento AND e.documento = ?";

    private static final String SQL_INSERT_EMPLEADO = "INSERT INTO empleados (documento,cargo,estado) VALUES (?,?,?);";
    
    private static final String SQL_INSERT_PERSONA = "INSERT INTO personas (documento,nombre,apellido,fecha_nacimiento,direccion,telefono,email) VALUES (?,?,?,date(?),?,?,?);";

    private static final String SQL_UPDATE_EMPLEADO = "UPDATE empleados SET  cargo =?, estado =? WHERE documento = ?";
    
    private static final String SQL_UPDATE_PERSONA = "UPDATE personas SET  nombre =?, apellido =?, fecha_nacimiento = date(?), direccion =?, telefono =?, email =? WHERE documento = ?";

    private static final String SQL_DElETE_EMPLEADO = "DELETE FROM empleados WHERE documento = ?";
    
    private static final String SQL_DElETE_PERSONA = "DELETE FROM personas WHERE documento = ?";

    public EmpleadoJDBC() {
    }

    public EmpleadoJDBC(Connection conexionTransaccional) {
        this.conexionTransaccional = conexionTransaccional;
    }

    @Override
    public List<Empleado> listAll() throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Empleado empleado = null;
        List<Empleado> empleados = new ArrayList<Empleado>();
        
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_LIST_ALL);
            rs = stmt.executeQuery();
            while (rs.next()) {
                String cargo = rs.getString("cargo");
                boolean estado = rs.getBoolean("estado");
                int documento = rs.getInt("documento");
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                Date fechaNacimiento = rs.getDate("fecha_nacimiento");
                String direccion = rs.getString("direccion");
                String telefono = rs.getString("telefono");
                String email = rs.getString("email");
   
                empleado = new Empleado();
                
                empleado.setCargo(cargo);
                empleado.setEstado(estado);
                empleado.setDocumento(documento);
                empleado.setNombre(nombre);
                empleado.setApellido(apellido);
                empleado.setFechaNacimiento(fechaNacimiento);
                empleado.setDireccion(direccion);
                empleado.setTelefono(telefono);
                empleado.setEmail(email);
                
            }
        } finally {
            
            Conexion.close(rs);
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return empleados;
    }
    
    @Override
    public Integer insert(Empleado empleado) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            
            this.insertPersona(empleado);
            
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT_EMPLEADO);
            stmt.setInt(1, empleado.getDocumento());
            stmt.setString(2, empleado.getCargo().toUpperCase());
            stmt.setBoolean(3, empleado.getEstado());

            System.out.println("Ejecutando Query: " + SQL_INSERT_EMPLEADO);
            rows = stmt.executeUpdate();
            System.out.println("Registros afectados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }
    private Integer insertPersona(Empleado empleado) throws SQLException{
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;
        
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT_PERSONA);
            stmt.setInt(1, empleado.getDocumento());
            stmt.setString(2, empleado.getNombre().toUpperCase());
            stmt.setString(3, empleado.getApellido().toUpperCase());
            stmt.setString(4, Utils.getStringFromDate(empleado.getFechaNacimiento()));
            stmt.setString(5, empleado.getDireccion().toUpperCase());
            stmt.setString(6, empleado.getTelefono());
            stmt.setString(7, empleado.getEmail().toUpperCase());
            
            System.out.println("Ejecutando Query: " + SQL_INSERT_PERSONA);
            rows = stmt.executeUpdate();
            System.out.println("Registros afectados: " + rows);
            
        }finally {
            //conn.setAutoCommit(false);
            //conn.commit();
            
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }
     @Override
    public Integer update(Empleado empleado) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            this.updatePersona(empleado);
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            System.out.println("Ejecutando Query: " + SQL_UPDATE_EMPLEADO);
            stmt = conn.prepareStatement(SQL_UPDATE_EMPLEADO);
 
            stmt.setString(1, empleado.getCargo().toUpperCase());
            stmt.setBoolean(2, empleado.getEstado());
            stmt.setInt(3, empleado.getDocumento());

            rows = stmt.executeUpdate();
            System.out.println("Registros actualizados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }
    private Integer updatePersona(Empleado empleado) throws SQLException{
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;
        
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
           
            System.out.println("Ejecutando Query: " + SQL_UPDATE_PERSONA);
            stmt = conn.prepareStatement(SQL_UPDATE_PERSONA);
            System.out.println("Ejecutando Query: " + SQL_UPDATE_PERSONA);
            stmt.setString(1, empleado.getNombre().toUpperCase());
            stmt.setString(2, empleado.getApellido().toUpperCase());
            stmt.setString(3, Utils.getStringFromDate(empleado.getFechaNacimiento()));
            stmt.setString(4, empleado.getDireccion().toUpperCase());
            stmt.setString(5, empleado.getTelefono());
            stmt.setString(6, empleado.getEmail().toUpperCase());
            stmt.setInt(7, empleado.getDocumento());
            
            rows = stmt.executeUpdate();
            System.out.println("Registros actualizados: " + rows);
            
        }finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }

    @Override
    public Integer delete(Empleado empleado) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            System.out.println("Ejecutando Query: " + SQL_DElETE_EMPLEADO);
            stmt = conn.prepareStatement(SQL_DElETE_EMPLEADO);
            stmt.setInt(1, empleado.getDocumento());
            rows = stmt.executeUpdate();
            System.out.println("Registros eliminados: " + rows);
            this.deletePersona(empleado);
            
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }
   
    private Integer deletePersona(Empleado empleado) throws SQLException{
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;
         try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_DElETE_PERSONA);
            stmt.setInt(1, empleado.getDocumento());
            rows = stmt.executeUpdate();
            System.out.println("Registros eliminados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }    
   
    @Override
    public Empleado getEmpleado(Integer documento) throws SQLException {
         Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Empleado empleado = null;

        conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
        stmt = conn.prepareStatement(SQL_SELECT_EMPLEADO_POR_DOCUMENTO);
        stmt.setInt(1, documento);
        rs = stmt.executeQuery();

        if (rs.next()) {
            empleado = new Empleado();
            empleado.setDocumento(rs.getInt("documento"));
            empleado.setCargo(rs.getString("cargo"));
            empleado.setEstado(rs.getBoolean("estado"));
            empleado.setNombre(rs.getString("nombre"));
            empleado.setApellido(rs.getString("apellido"));
            empleado.setFechaNacimiento(rs.getDate("fecha_nacimiento"));
            empleado.setDireccion(rs.getString("direccion"));
            empleado.setTelefono(rs.getString("telefono"));
            empleado.setEmail(rs.getString("email"));

           return (empleado);
        }
        return (null);
    }
    @Override
    public List<Empleado> filter(EmpleadoFilter filter) throws SQLException {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Empleado empleado = null;
        List<Empleado> empleados = new ArrayList<Empleado>();
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            String query = this.addFilters(SQL_LIST_ALL, filter);    
            System.out.println("Ejecutando Query: " + query);
            stmt = conn.prepareStatement(query);            
            while (rs.next()) {
                String cargo = rs.getString("cargo");
                boolean estado = rs.getBoolean("estado");
                int documento = rs.getInt("documento");
                String nombre = rs.getString("nombre");
                String apellido = rs.getString("apellido");
                Date fechaNacimiento = rs.getDate("fecha_nacimiento");
                String direccion = rs.getString("direccion");
                String telefono = rs.getString("telefono");
                String email = rs.getString("email");
   
                empleado = new Empleado();
                
                empleado.setCargo(cargo);
                empleado.setEstado(estado);
                empleado.setDocumento(documento);
                empleado.setNombre(nombre);
                empleado.setApellido(apellido);
                empleado.setFechaNacimiento(fechaNacimiento);
                empleado.setDireccion(direccion);
                empleado.setTelefono(telefono);
                empleado.setEmail(email);
                
                empleados.add(empleado);
            }
        } finally {
            Conexion.close(rs);
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }

        return empleados;
        
    }

   private String addFilters(String query, EmpleadoFilter filter){
       if(filter == null)
           return query;
       
       String filtrosWhere = "";
       
       if(filter.getDocumento() != null){
           filtrosWhere += "documento = " + filter.getDocumento();
       }
       
       if(filter.getCargo() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "cargo = " + filter.getCargo();
       }
       
       if(filter.getEstado() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "estado = " + filter.getEstado();
       }
       
       if(filter.getNombre() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "nombre = " + filter.getNombre();
       }
       
       if(filter.getApellido() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "apellido = " + filter.getApellido();
       }
       
       if(filter.getFechaNacimiento() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "fecha_nacimiento = " + filter.getFechaNacimiento();
       }
       
       if(filter.getDireccion() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "direccion = " + filter.getDireccion();
       }
       
       if(filter.getTelefono()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "telefono = " + filter.getTelefono();
       }
       
       if(filter.getEmail()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "email = " + filter.getEmail();
       }
       
       if(!filtrosWhere.isEmpty()){
           query = query + " WHERE " + filtrosWhere;
       }
       
       return query;
   }
    
}
