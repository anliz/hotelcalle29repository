package capa_datos;

import capa_modelo.Cliente;
import filter.ClienteFilter;
import java.sql.SQLException;
import java.util.List;


public interface ClienteDao {
    
     
    List<Cliente> listAll() throws SQLException;
    
    Cliente getCliente(Integer documento) throws SQLException;
    
    Integer insert(Cliente cliente) throws SQLException;
    
    Integer update(Cliente cliente) throws SQLException;
    
    Integer delete(Cliente cliente) throws SQLException;
    
    List<Cliente> filter(ClienteFilter filter) throws SQLException;
    
    
}
