package capa_datos;

import capa_modelo.Reserva;
import filter.ReservaFilter;
import java.sql.SQLException;
import java.util.List;

public interface ReservaDao {
    
    List<Reserva> listAll() throws SQLException;
    
    Reserva getReserva(Integer codigo) throws SQLException;
    
    Integer insert(Reserva reserva) throws SQLException;
    
    Integer update(Reserva reserva) throws SQLException;
    
    Integer delete(Reserva reserva) throws SQLException;
    
    List<Reserva> filter(ReservaFilter filter) throws SQLException;
}
