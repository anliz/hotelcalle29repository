package capa_datos;

import conexion.Conexion;
import capa_modelo.*;
import filter.HabitacionFilter;
import filter.TipoHabitacionFilter;
import java.sql.*;
import java.util.*;

public class TipoHabitacionJDBC implements TipoHabitacionDao{
    
    private Connection conexionTransaccional;

    private static final String SQL_LIST_ALL = "SELECT codigo,descripcion,tarifa1,tarifa2,"
            + "tarifa3,adicional,estado FROM tipo_habitaciones";

    private static final String SQL_SELECT_TIPO_HABITACION_POR_CODIGO = "SELECT codigo,descripcion,tarifa1,tarifa2,"
            + "tarifa3,adicional,estado FROM tipo_habitaciones WHERE codigo = ?";

    private static final String SQL_INSERT = "INSERT INTO tipo_habitaciones (codigo,descripcion,tarifa1,tarifa2,"
            + "tarifa3,adicional,estado) VALUES (?,?,?,?,?,?,?)";

    private static final String SQL_UPDATE = "UPDATE tipo_habitaciones SET descripcion =?,tarifa1 =? ,tarifa2 =?,"
            + "tarifa3 =? ,adicional =? ,estado =? WHERE codigo = ?";

    private static final String SQL_DElETE = "DELETE FROM tipo_habitaciones WHERE codigo = ?";
    

    public TipoHabitacionJDBC() {
    }

    public TipoHabitacionJDBC(Connection conexionTransaccional) {
        this.conexionTransaccional = conexionTransaccional;
    }
    
    
    @Override
    public List<TipoHabitacion> listAll() throws SQLException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        TipoHabitacion tipoHabitacion = null;
        List<TipoHabitacion> tipoHabitaciones = new ArrayList<TipoHabitacion>();
        
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_LIST_ALL);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int codigo = rs.getInt("codigo");
                String descripcion = rs.getString("descripcion");
                double tarifa1 = rs.getDouble("tarifa1");
                double tarifa2 = rs.getDouble("tarifa2");
                double tarifa3 = rs.getDouble("tarifa3");
                double adicional = rs.getDouble("adicional");
                boolean estado = rs.getBoolean("estado");
                
                tipoHabitacion = new TipoHabitacion();
                tipoHabitacion.setCodigo(codigo);
                tipoHabitacion.setDescripcion(descripcion);
                tipoHabitacion.setTarifa1(tarifa1);
                tipoHabitacion.setTarifa2(tarifa2);
                tipoHabitacion.setTarifa3(tarifa3);
                tipoHabitacion.setAdicional(adicional);
                tipoHabitacion.setEstado(estado);
                
                tipoHabitaciones.add(tipoHabitacion);
            }
        } finally {
            Conexion.close(rs);
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return tipoHabitaciones;
    }
    
    @Override
    public Integer insert(TipoHabitacion tipoHabitacion) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
            stmt.setInt(1, tipoHabitacion.getCodigo());
            stmt.setString(2, tipoHabitacion.getDescripcion().toUpperCase());
            stmt.setDouble(3, tipoHabitacion.getTarifa1());
            stmt.setDouble(4, tipoHabitacion.getTarifa2());
            stmt.setDouble(5, tipoHabitacion.getTarifa3());
            stmt.setDouble(6, tipoHabitacion.getAdicional());
            stmt.setBoolean(7, tipoHabitacion.isEstado());
            
            System.out.println("Ejecutando Query: " + SQL_INSERT);
            rows = stmt.executeUpdate();
            System.out.println("Registros afectados" + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }

    @Override
    public Integer update(TipoHabitacion tipoHabitacion) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            System.out.println("Ejecutando Query: " + SQL_UPDATE);
            stmt = conn.prepareStatement(SQL_UPDATE);  
            stmt.setString(1, tipoHabitacion.getDescripcion().toUpperCase());
            stmt.setDouble(2, tipoHabitacion.getTarifa1());
            stmt.setDouble(3, tipoHabitacion.getTarifa2());
            stmt.setDouble(4, tipoHabitacion.getTarifa3());
            stmt.setDouble(5, tipoHabitacion.getAdicional());
            stmt.setBoolean(6, tipoHabitacion.isEstado());
            stmt.setInt(7, tipoHabitacion.getCodigo());
            

            rows = stmt.executeUpdate();
            System.out.println("Registros actualizados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }

    @Override
    public Integer delete(TipoHabitacion tipoHabitacion) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            System.out.println("Ejecutando Query: " + SQL_DElETE);
            stmt = conn.prepareStatement(SQL_DElETE);
            stmt.setInt(1, tipoHabitacion.getCodigo());
            rows = stmt.executeUpdate();
            System.out.println("Registros eliminados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }

    @Override
    public TipoHabitacion getTipoHabitacion(Integer codigo) throws SQLException {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
        stmt = conn.prepareStatement(SQL_SELECT_TIPO_HABITACION_POR_CODIGO);
        stmt.setInt(1, codigo);
        rs = stmt.executeQuery();

        if (rs.next()) {
            TipoHabitacion tipoHabitacion = new TipoHabitacion();
            tipoHabitacion.setCodigo(rs.getInt("codigo"));
            tipoHabitacion.setDescripcion(rs.getString("descripcion"));
            tipoHabitacion.setTarifa1(rs.getInt("tarifa1"));
            tipoHabitacion.setTarifa2(rs.getInt("tarifa2"));
            tipoHabitacion.setTarifa3(rs.getInt("tarifa3"));
            tipoHabitacion.setAdicional(rs.getDouble("adicional"));
            tipoHabitacion.setEstado(rs.getBoolean("estado"));

            return(tipoHabitacion);
        }
        return (null);
    }
    
    @Override
    public List<TipoHabitacion> filter(TipoHabitacionFilter filter) throws SQLException {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        TipoHabitacion tipoHabitacion = null;
        List<TipoHabitacion> tipoHabitaciones = new ArrayList<TipoHabitacion>();
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            
            String query = this.addFilters(SQL_LIST_ALL, filter);    
            System.out.println("Ejecutando Query: " + query);
            stmt = conn.prepareStatement(query);            
            
            rs = stmt.executeQuery();
            while (rs.next()) {
                int codigo = rs.getInt("codigo");
                String descripcion = rs.getString("descripcion");
                double tarifa1 = rs.getDouble("tarifa1");
                double tarifa2 = rs.getDouble("tarifa2");
                double tarifa3 = rs.getDouble("tarifa3");
                double adicional = rs.getDouble("adicional");
                boolean estado = rs.getBoolean("estado");
                
                tipoHabitacion = new TipoHabitacion();
                tipoHabitacion.setCodigo(codigo);
                tipoHabitacion.setDescripcion(descripcion);
                tipoHabitacion.setTarifa1(tarifa1);
                tipoHabitacion.setTarifa2(tarifa2);
                tipoHabitacion.setTarifa3(tarifa3);
                tipoHabitacion.setAdicional(adicional);
                tipoHabitacion.setEstado(estado);
                
                tipoHabitaciones.add(tipoHabitacion);
                
            }
        } finally {
            Conexion.close(rs);
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }

        return tipoHabitaciones;
        
    }

   private String addFilters(String query, TipoHabitacionFilter filter){
       if(filter == null)
           return query;
       
       String filtrosWhere = "";
       
       if(filter.getCodigo() != null){
           filtrosWhere += "codigo = " + filter.getCodigo();
       }
       
       if(filter.getDescripcion() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "descripcion = " + filter.getDescripcion();
       }
       
       if(filter.getTarifa1() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "tarifa1 = " + filter.getTarifa1();
       }
       
       if(filter.getTarifa2() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "tarifa2 = " + filter.getTarifa2();
       }
       
       if(filter.getTarifa3() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "tarifa3 = " + filter.getTarifa3();
       }
       
       if(filter.getAdicional() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "adicional = " + filter.getAdicional();
       }
       
       if(filter.getEstado() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "estado = " + filter.getEstado();
       }
       
       if(!filtrosWhere.isEmpty()){
           query = query + " WHERE " + filtrosWhere;
       }
       
       return query;
   }

    
}
