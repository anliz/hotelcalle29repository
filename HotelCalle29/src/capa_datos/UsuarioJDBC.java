package capa_datos;

import conexion.Conexion;
import capa_modelo.Empleado;
import capa_modelo.Usuario;
import filter.UsuarioFilter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UsuarioJDBC implements UsuarioDao {
    
     private Connection conexionTransaccional;

    private static final String SQL_LIST_ALL = "SELECT id, contrasena, documento_empleado FROM usuarios";

    private static final String SQL_SELECT_USUARIO_POR_ID = "SELECT id,contrasena, documento_empleado FROM usuarios WHERE id = ?";
    
    private static final String SQL_SELECT_EMPLEADO_POR_DOCUMENTO = "SELECT   e.cargo, e.estado, p.documento, p.nombre, p.apellido, p.fecha_nacimiento,"
            + " p.direccion, p.telefono, p.email FROM empleados as e, personas as p WHERE e.documento = p.documento AND e.documento = ?";

    private static final String SQL_INSERT = "INSERT INTO usuarios (id, contrasena, documento_empleado) VALUES (?,?,?)";

    private static final String SQL_UPDATE = "UPDATE usuarios SET  contrasena = ?, documento_empleado = ? WHERE id = ?";

    private static final String SQL_DElETE = "DELETE FROM usuarios WHERE id = ?";

    public UsuarioJDBC() {
    }

    public UsuarioJDBC(Connection conexionTransaccional) {
        this.conexionTransaccional = conexionTransaccional;
    }

    @Override
    public List<Usuario> listAll() throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Usuario usuario = null;
        List<Usuario> usuarios = new ArrayList<Usuario>();
        
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_LIST_ALL);
            rs = stmt.executeQuery();
            while (rs.next()) {
                String id = rs.getString("id");
                String contrasena = rs.getString("contrasena");
                int documentoEmpleado = rs.getInt("documento_empleado");
                
                usuario = new Usuario();
                usuario.setId(id);
                usuario.setContrasena(contrasena);
                Empleado empleado = getEmpleadoPorDocumento(documentoEmpleado);
                usuario.setEmpleado(empleado);
                usuarios.add(usuario);
            }
        } finally {
            Conexion.close(rs);
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return usuarios;
    }

    @Override
    public Integer insert(Usuario usuario) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
            stmt.setString(1, usuario.getId());
            stmt.setString(2, usuario.getContrasena());
            stmt.setInt(3, usuario.getEmpleado().getDocumento());

            System.out.println("Ejecutando Query: " + SQL_INSERT);
            rows = stmt.executeUpdate();
            System.out.println("Registros afectados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }

    @Override
    public Integer update(Usuario usuario) throws SQLException {
       Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            System.out.println("Ejecutando Query: " + SQL_UPDATE);
            stmt = conn.prepareStatement(SQL_UPDATE);
            
            
            stmt.setString(1, usuario.getContrasena());
            stmt.setInt(2, usuario.getEmpleado().getDocumento());            
            stmt.setString(3, usuario.getId());

            rows = stmt.executeUpdate();
            System.out.println("Registros actualizados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }

    @Override
    public Integer delete(Usuario usuario) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            System.out.println("Ejecutando Query: " + SQL_DElETE);
            stmt = conn.prepareStatement(SQL_DElETE);
            stmt.setString(1, usuario.getId());
            rows = stmt.executeUpdate();
            System.out.println("Registros eliminados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }
    
    @Override
    public Usuario getUsuario(String id) throws SQLException {
         Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
        stmt = conn.prepareStatement(SQL_SELECT_USUARIO_POR_ID);
        stmt.setString(1, id);
        rs = stmt.executeQuery();

        if (rs.next()) {
            Usuario usuario = new Usuario();
            usuario.setId(rs.getString("id"));
            usuario.setContrasena(rs.getString("contrasena"));
            int documentoEmpleado = rs.getInt("documento_empleado");
            Empleado empleado = getEmpleadoPorDocumento(documentoEmpleado);
            usuario.setEmpleado(empleado);
            return (usuario);
        }
        return (null);
    }


    private Empleado getEmpleadoPorDocumento(int documento) throws SQLException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
        stmt = conn.prepareStatement(SQL_SELECT_EMPLEADO_POR_DOCUMENTO);
        stmt.setInt(1, documento);
        rs = stmt.executeQuery();

        if (rs.next()) {
            Empleado empleado = new Empleado();

            //Eliminemos los ALIAS, para que el query los tome bien las columnas
            
            empleado.setCargo(rs.getString("cargo"));
            empleado.setEstado(rs.getBoolean("estado"));
            empleado.setDocumento(rs.getInt("documento"));
            empleado.setNombre(rs.getString("nombre"));
            empleado.setApellido(rs.getString("apellido"));
            empleado.setFechaNacimiento(rs.getDate("fecha_nacimiento"));
            empleado.setDireccion(rs.getString("direccion"));
            empleado.setTelefono(rs.getString("telefono"));
            empleado.setEmail(rs.getString("email"));

            return (empleado);
        }
        return (null);
    }
    
    @Override
    public List<Usuario> filter(UsuarioFilter filter) throws SQLException {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Usuario usuario = null;
        List<Usuario> usuarios = new ArrayList<Usuario>();
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();

            String query = this.addFilters(SQL_LIST_ALL, filter);    
            System.out.println("Ejecutando Query: " + query);
            stmt = conn.prepareStatement(query);            
            
            rs = stmt.executeQuery();
            while (rs.next()) {
                String id = rs.getString("id");
                String contrasena = rs.getString("contrasena");
                int documentoEmpleado = rs.getInt("documento_empleado");
                
                usuario = new Usuario();
                usuario.setId(id);
                usuario.setContrasena(contrasena);
                Empleado empleado = getEmpleadoPorDocumento(documentoEmpleado);
                usuario.setEmpleado(empleado);
                usuarios.add(usuario);
            }
        } finally {
            Conexion.close(rs);
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return usuarios;     
    }

   private String addFilters(String query, UsuarioFilter filter){
       if(filter == null)
           return query;
       
       String filtrosWhere = "";
       
       if(filter.getId() != null){
           filtrosWhere += "id = " + filter.getId();
       }
       
       if(filter.getContrasena() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "contrasena = " + filter.getContrasena();
       }
       
       if(filter.getDocumentoEmpleado() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "documento_empleado = " + filter.getDocumentoEmpleado();
       }
      
       if(!filtrosWhere.isEmpty()){
           query = query + " WHERE " + filtrosWhere;
       }
       
       return query;
   }
    
}
