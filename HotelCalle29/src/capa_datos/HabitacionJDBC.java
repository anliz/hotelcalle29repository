package capa_datos;

import conexion.Conexion;
import filter.HabitacionFilter;
import capa_modelo.*;
import java.sql.*;
import java.util.*;

public class HabitacionJDBC implements HabitacionDao {

    private Connection conexionTransaccional;

    private static final String SQL_LIST_ALL = "SELECT numero,numero_planta,"
            + "num_cama_sencilla,num_cama_doble,estado_ocupacion,estado,codigo_tipo_habitacion FROM habitaciones";

    private static final String SQL_SELECT_HABITACION_POR_NUMERO = "SELECT numero,numero_planta,"
            + "num_cama_sencilla,num_cama_doble,estado_ocupacion,estado,codigo_tipo_habitacion FROM habitaciones WHERE numero = ?";

    private static final String SQL_SELECT_TIPO_HABITACION_POR_CODIGO = "SELECT codigo,descripcion,tarifa1,tarifa2,"
            + "tarifa3,adicional,estado FROM tipo_habitaciones WHERE codigo = ?";

    private static final String SQL_INSERT = "INSERT INTO habitaciones (numero,numero_planta,"
            + "num_cama_sencilla,num_cama_doble,estado_ocupacion,estado,codigo_tipo_habitacion) VALUES (?,?,?,?,?,?,?)";

    private static final String SQL_UPDATE = "UPDATE habitaciones SET numero_planta = ?,num_cama_sencilla = ?,"
            + "num_cama_doble = ?,estado_ocupacion = ?,estado = ?,codigo_tipo_habitacion = ? WHERE numero = ?";

    private static final String SQL_DElETE = "DELETE FROM habitaciones WHERE numero = ?";

    public HabitacionJDBC() {
    }

    public HabitacionJDBC(Connection conexionTransaccional) {
        this.conexionTransaccional = conexionTransaccional;
    }
    
    @Override
    public List<Habitacion> listAll() throws SQLException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Habitacion habitacion = null;
        List<Habitacion> habitaciones = new ArrayList<Habitacion>();
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_LIST_ALL);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int numero = rs.getInt("numero");
                int numeroPlanta = rs.getInt("numero_planta");
                int numeroCamaSencilla = rs.getInt("num_cama_sencilla");
                int numeroCamaDoble = rs.getInt("num_cama_doble");
                String estadoOcupacion = rs.getString("estado_ocupacion");
                boolean estado = rs.getBoolean("estado");
                int codigoTipoHabitacion = rs.getInt("codigo_tipo_habitacion");

                habitacion = new Habitacion();
                habitacion.setNumero(numero);
                habitacion.setNumeroPlanta(numeroPlanta);
                habitacion.setNumeroCamaSencilla(numeroCamaSencilla);
                habitacion.setNumeroCamaDoble(numeroCamaDoble);
                habitacion.setEstadoOcupacion(estadoOcupacion);
                habitacion.setEstado(estado);
                TipoHabitacion tipoHabitacion = getTipoHabitacionPorCodigo(codigoTipoHabitacion);
                habitacion.setTipoHabitacion(tipoHabitacion);

                habitaciones.add(habitacion);
            }
        } finally {
            Conexion.close(rs);
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }

        return habitaciones;
    }

    @Override
    public Integer insert(Habitacion habitacion) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
            stmt.setInt(1, habitacion.getNumero());
            stmt.setInt(2, habitacion.getNumeroPlanta());
            stmt.setInt(3, habitacion.getNumeroCamaSencilla());
            stmt.setInt(4, habitacion.getNumeroCamaDoble());
            stmt.setString(5, habitacion.getEstadoOcupacion());
            stmt.setBoolean(6, habitacion.isEstado());
            stmt.setInt(7, habitacion.getTipoHabitacion().getCodigo());

            System.out.println("Ejecutando Query: " + SQL_INSERT);
            rows = stmt.executeUpdate();
            System.out.println("Registros afectados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }

    @Override
    public Integer update(Habitacion habitacion) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            System.out.println("Ejecutando Query: " + SQL_UPDATE);
            stmt = conn.prepareStatement(SQL_UPDATE);
            
            //stmt.setInt(1, habitacion.getNumero());
            stmt.setInt(1, habitacion.getNumeroPlanta());
            stmt.setInt(2, habitacion.getNumeroCamaSencilla());
            stmt.setInt(3, habitacion.getNumeroCamaDoble());
            stmt.setString(4, habitacion.getEstadoOcupacion());
            stmt.setBoolean(5, habitacion.isEstado());
            stmt.setInt(6, habitacion.getTipoHabitacion().getCodigo());            
            stmt.setInt(7, habitacion.getNumero());

            rows = stmt.executeUpdate();
            System.out.println("Registros actualizados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }

    @Override
    public Integer delete(Habitacion habitacion) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            System.out.println("Ejecutando Query: " + SQL_DElETE);
            stmt = conn.prepareStatement(SQL_DElETE);
            stmt.setInt(1, habitacion.getNumero());
            rows = stmt.executeUpdate();
            System.out.println("Registros eliminados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }
    
    private TipoHabitacion getTipoHabitacionPorCodigo(int codigo) throws SQLException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
        stmt = conn.prepareStatement(SQL_SELECT_TIPO_HABITACION_POR_CODIGO);
        stmt.setInt(1, codigo);
        rs = stmt.executeQuery();

        if (rs.next()) {
            TipoHabitacion tipoHabitacion = new TipoHabitacion();
            tipoHabitacion.setCodigo(rs.getInt("codigo"));
            tipoHabitacion.setDescripcion(rs.getString("descripcion"));
            tipoHabitacion.setTarifa1(rs.getInt("tarifa1"));
            tipoHabitacion.setTarifa2(rs.getInt("tarifa2"));
            tipoHabitacion.setTarifa3(rs.getInt("tarifa3"));
            tipoHabitacion.setAdicional(rs.getDouble("adicional"));
            tipoHabitacion.setEstado(rs.getBoolean("estado"));

            return (tipoHabitacion);
        }
        return (null);
    }

    @Override
    public Habitacion getHabitacion(Integer numero) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Habitacion habitacion = null;

        conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
        stmt = conn.prepareStatement(SQL_SELECT_HABITACION_POR_NUMERO);
        stmt.setInt(1, numero);
        rs = stmt.executeQuery();

        if (rs.next()) {
            habitacion = new Habitacion();
            habitacion.setNumero(rs.getInt("numero"));
            habitacion.setNumeroPlanta(rs.getInt("numero_planta"));
            habitacion.setNumeroCamaSencilla(rs.getInt("num_cama_sencilla"));
            habitacion.setNumeroCamaDoble(rs.getInt("num_cama_doble"));
            habitacion.setEstadoOcupacion(rs.getString("estado_ocupacion"));
            habitacion.setEstado(rs.getBoolean("estado"));
            int codigoTipoHabitacion = rs.getInt("codigo_tipo_habitacion");
            TipoHabitacion tipoHabitacion = getTipoHabitacionPorCodigo(codigoTipoHabitacion);
            habitacion.setTipoHabitacion(tipoHabitacion);

           return (habitacion);
        }
        return (null);
    }

    @Override
    public List<Habitacion> filter(HabitacionFilter filter) throws SQLException {
        
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Habitacion habitacion = null;
        List<Habitacion> habitaciones = new ArrayList<Habitacion>();
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            
            String query = this.addFilters(SQL_LIST_ALL, filter);    
            System.out.println("Ejecutando Query: " + query);
            stmt = conn.prepareStatement(query);            
            
            rs = stmt.executeQuery();
            while (rs.next()) {
                int numero = rs.getInt("numero");
                int numeroPlanta = rs.getInt("numero_planta");
                int numeroCamaSencilla = rs.getInt("num_cama_sencilla");
                int numeroCamaDoble = rs.getInt("num_cama_doble");
                String estadoOcupacion = rs.getString("estado_ocupacion");
                boolean estado = rs.getBoolean("estado");
                int codigoTipoHabitacion = rs.getInt("codigo_tipo_habitacion");

                habitacion = new Habitacion();
                habitacion.setNumero(numero);
                habitacion.setNumeroPlanta(numeroPlanta);
                habitacion.setNumeroCamaSencilla(numeroCamaSencilla);
                habitacion.setNumeroCamaDoble(numeroCamaDoble);
                habitacion.setEstadoOcupacion(estadoOcupacion);
                habitacion.setEstado(estado);
                TipoHabitacion tipoHabitacion = getTipoHabitacionPorCodigo(codigoTipoHabitacion);
                habitacion.setTipoHabitacion(tipoHabitacion);

                habitaciones.add(habitacion);
            }
        } finally {
            Conexion.close(rs);
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }

        return habitaciones;
        
    }

   private String addFilters(String query, HabitacionFilter filter){
       if(filter == null)
           return query;
       
       String filtrosWhere = "";
       
       if(filter.getNumero() != null){
           filtrosWhere += "numero = " + filter.getNumero();
       }
       
       if(filter.getNumeroPlanta() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "numero_planta = " + filter.getNumeroPlanta();
       }
       
       if(filter.getNumeroCamaSencilla() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "num_cama_sencilla = " + filter.getNumeroCamaSencilla();
       }
       
       if(filter.getNumeroCamaDoble() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "num_cama_doble = " + filter.getNumeroCamaDoble();
       }
       
       if(filter.getEstadoOcupacion() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "estado_ocupacion = " + filter.getEstadoOcupacion();
       }
       
       if(filter.getEstado() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "estado = " + filter.getEstado();
       }
       
       if(filter.getCodigoTipoHabitacion() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "codigo_tipo_habitacion = " + filter.getCodigoTipoHabitacion();
       }
       
       if(!filtrosWhere.isEmpty()){
           query = query + " WHERE " + filtrosWhere;
       }
       
       return query;
   }

   
}
