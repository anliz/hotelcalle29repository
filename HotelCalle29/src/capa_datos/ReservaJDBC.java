package capa_datos;

import capa_modelo.Cliente;
import capa_modelo.Empleado;
import capa_modelo.Reserva;
import capa_modelo.Usuario;
import capa_negocio.Utils;
import conexion.Conexion;
import filter.ReservaFilter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReservaJDBC implements ReservaDao {
    
     private Connection conexionTransaccional;

    private static final String SQL_LIST_ALL = "SELECT codigo,fecha,fecha_entrada,fecha_salida,"
            + "cantidad_personas,estado,documento_cliente,id_usuario,tipo_tarifa FROM reservas";

    private static final String SQL_SELECT_RESERVA_POR_CODIGO = "SELECT codigo,fecha,fecha_entrada,fecha_salida"
            + "cantidad_personas,estado,documento_cliente,id_usuario,tipo_tarifa FROM reservas codigo = ?";

    private static final String SQL_SELECT_CLIENTE_POR_DOCUMENTO = "SELECT c.lugar_expedicion,c.ciudad,c.pais,"
            + "c.ocupacion, c.estado, p.documento, p.nombre, p.apellido, p.fecha_nacimiento,p.direccion, p.telefono,"
            + " p.email FROM clientes as c, personas as p WHERE c.documento = p.documento AND c.documento = ?";
    
    private static final String SQL_SELECT_USUARIO_POR_ID = "SELECT id, contrasena, documento_empleado FROM usuarios WHERE id =?";
    
    private static final String SQL_SELECT_EMPLEADO_POR_DOCUMENTO = "SELECT e.cargo, e.estado, p.documento, p.nombre, p.apellido, p.fecha_nacimiento,"
            + " p.direccion, p.telefono, p.email FROM empleados as e, personas as p WHERE e.documento = p.documento AND e.documento = ?";
    
    private static final String SQL_INSERT = "INSERT INTO reservas (codigo,fecha,fecha_entrada,fecha_salida," 
           + "cantidad_personas,estado,documento_cliente,id_usuario,tipo_tarifa) VALUES (?,date(?),date(?),date(?),?,?,?,?,?)";

    private static final String SQL_UPDATE = "UPDATE reservas SET fecha = date(?), fecha_entrada = date(?), fecha_salida = date(?)," 
           + "cantidad_personas = ?,estado = ?,documento_cliente = ?,id_usuario = ?,tipo_tarifa = ? WHERE codigo = ?";

    private static final String SQL_DElETE = "DELETE FROM reservas WHERE codigo = ?";

    public ReservaJDBC() {
    }

    public ReservaJDBC(Connection conexionTransaccional) {
        this.conexionTransaccional = conexionTransaccional;
    }

    @Override
    public List<Reserva> listAll() throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Reserva reserva = null;
        List<Reserva> reservas = new ArrayList<Reserva>();
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_LIST_ALL);
            rs = stmt.executeQuery();
            while (rs.next()) {
                int codigo = rs.getInt("codigo");
                Date fecha = rs.getDate("fecha");
                Date fechaEntrada = rs.getDate("fecha_entrada");
                Date fechaSalida = rs.getDate("fecha_salida");
                int cantidaPersonas = rs.getInt("cantidad_personas");
                boolean estado = rs.getBoolean("estado");
                int documentoCliente = rs.getInt("documento_cliente");
                String idUsuario = rs.getString("id_usuario");
                String tipoTarifa = rs.getString("tipo_tarifa");

                reserva = new Reserva();
                reserva.setCodigo(codigo);
                reserva.setFecha(fecha);
                reserva.setFechaEntrada(fechaEntrada);
                reserva.setFechaSalida(fechaSalida);
                reserva.setCantidadPersonas(cantidaPersonas);
                reserva.setEstado(estado);
                Cliente cliente = getClientePorDocumento(documentoCliente);
                reserva.setCliente(cliente);
                Usuario usuario = getUsuarioPorId(idUsuario);
                reserva.setUsuario(usuario);
                reserva.setTipoTarifa(tipoTarifa);

                reservas.add(reserva);
            }
        } finally {
            Conexion.close(rs);
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }

        return reservas;
    }

    @Override
    public Reserva getReserva(Integer codigo) throws SQLException {
       Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Reserva reserva = null;

        conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
        stmt = conn.prepareStatement(SQL_SELECT_RESERVA_POR_CODIGO);
        stmt.setInt(1, codigo);
        rs = stmt.executeQuery();

        if (rs.next()) {
            reserva = new Reserva();
            reserva.setCodigo(rs.getInt("codigo"));
            reserva.setFecha(rs.getDate("fecha"));
            reserva.setFechaEntrada(rs.getDate("fecha_entrada"));
            reserva.setFechaSalida(rs.getDate("fecha_salida"));
            reserva.setCantidadPersonas(rs.getInt("Cantidad_personas"));
            reserva.setEstado(rs.getBoolean("estado"));
            int documentoCliente = rs.getInt("documento_cliente");
            Cliente cliente = getClientePorDocumento(documentoCliente);
            reserva.setCliente(cliente);
            String idUsuario = rs.getString("id_usuario");
            Usuario usuario = getUsuarioPorId(idUsuario);
            reserva.setUsuario(usuario);
            reserva.setTipoTarifa(rs.getString("tipo_tarifa"));

           return (reserva);
        }
        return (null);
    }

    @Override
    public Integer insert(Reserva reserva) throws SQLException {
        Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            stmt = conn.prepareStatement(SQL_INSERT);
            stmt.setInt(1, reserva.getCodigo());
            stmt.setString(2, Utils.getStringFromDate(reserva.getFecha()));
            stmt.setString(3, Utils.getStringFromDate(reserva.getFechaEntrada()));
            stmt.setString(4, Utils.getStringFromDate(reserva.getFechaSalida()));
            stmt.setInt(5, reserva.getCantidadPersonas());
            stmt.setBoolean(6, reserva.isEstado());
            stmt.setInt(7, reserva.getCliente().getDocumento());
            stmt.setString(8, reserva.getUsuario().getId());
            stmt.setString(9, reserva.getTipoTarifa().toUpperCase());

            System.out.println("Ejecutando Query: " + SQL_INSERT);
            rows = stmt.executeUpdate();
            System.out.println("Registros afectados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }

    @Override
    public Integer update(Reserva reserva) throws SQLException {
         Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            System.out.println("Ejecutando Query: " + SQL_UPDATE);
            stmt = conn.prepareStatement(SQL_UPDATE);
            
            //stmt.setInt(1, habitacion.getNumero());
            stmt.setString(1, Utils.getStringFromDate(reserva.getFecha()));
            stmt.setString(2, Utils.getStringFromDate(reserva.getFechaEntrada()));
            stmt.setString(3, Utils.getStringFromDate(reserva.getFechaSalida()));
            stmt.setInt(4, reserva.getCantidadPersonas());
            stmt.setBoolean(5, reserva.isEstado());
            stmt.setInt(6, reserva.getCliente().getDocumento());
            stmt.setString(7, reserva.getUsuario().getId());
            stmt.setString(8, reserva.getTipoTarifa().toUpperCase());
            stmt.setInt(9, reserva.getCodigo());

            rows = stmt.executeUpdate();
            System.out.println("Registros actualizados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }

    @Override
    public Integer delete(Reserva reserva) throws SQLException {
         Connection conn = null;
        PreparedStatement stmt = null;
        int rows = 0;

        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            System.out.println("Ejecutando Query: " + SQL_DElETE);
            stmt = conn.prepareStatement(SQL_DElETE);
            stmt.setInt(1, reserva.getCodigo());
            rows = stmt.executeUpdate();
            System.out.println("Registros eliminados: " + rows);
        } finally {
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }
        return rows;
    }
    
    private Cliente getClientePorDocumento(int documento) throws SQLException {

        Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
        stmt = conn.prepareStatement(SQL_SELECT_CLIENTE_POR_DOCUMENTO);
        stmt.setInt(1, documento);
        rs = stmt.executeQuery();

        if (rs.next()) {
            
            Cliente cliente = new Cliente();
            cliente.setDocumento(rs.getInt("documento"));
            cliente.setLugarExpedicion("lugar_expedicion");
            cliente.setCiudad(rs.getString("ciudad"));
            cliente.setPais(rs.getString("pais"));
            cliente.setOcupacion(rs.getString("ocupacion"));
            cliente.setEstado(rs.getBoolean("estado"));
            cliente.setNombre(rs.getString("nombre"));
            cliente.setApellido(rs.getString("apellido"));
            cliente.setFechaNacimiento(rs.getDate("fecha_nacimiento"));
            cliente.setDireccion(rs.getString("direccion"));
            cliente.setTelefono(rs.getString("telefono"));
            cliente.setEmail(rs.getString("email"));

            return (cliente);
        }
        return (null);
    }
    private Usuario getUsuarioPorId(String id) throws SQLException {
         Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;

        conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
        stmt = conn.prepareStatement(SQL_SELECT_USUARIO_POR_ID);
        stmt.setString(1, id);
        rs = stmt.executeQuery();

        if (rs.next()) {
            Usuario usuario = new Usuario();
            usuario.setId(rs.getString("id"));
            usuario.setContrasena(rs.getString("contrasena"));
            int documentoEmpleado = rs.getInt("documento_empleado");
            Empleado empleado = getEmpleadoPorDocumento(documentoEmpleado);
            usuario.setEmpleado(empleado);
            return (usuario);
        }
        return (null);
    }
    private Empleado getEmpleadoPorDocumento(Integer documento) throws SQLException {
         Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Empleado empleado = null;

        conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
        stmt = conn.prepareStatement(SQL_SELECT_EMPLEADO_POR_DOCUMENTO);
        stmt.setInt(1, documento);
        rs = stmt.executeQuery();

        if (rs.next()) {
            empleado = new Empleado();
            empleado.setDocumento(rs.getInt("documento"));
            empleado.setCargo(rs.getString("cargo"));
            empleado.setEstado(rs.getBoolean("estado"));
            empleado.setNombre(rs.getString("nombre"));
            empleado.setApellido(rs.getString("apellido"));
            empleado.setFechaNacimiento(rs.getDate("fecha_nacimiento"));
            empleado.setDireccion(rs.getString("direccion"));
            empleado.setTelefono(rs.getString("telefono"));
            empleado.setEmail(rs.getString("email"));

           return (empleado);
        }
        return (null);
    }

    @Override
    public List<Reserva> filter(ReservaFilter filter) throws SQLException {
        
         Connection conn = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Reserva reserva = null;
        List<Reserva> reservas = new ArrayList<Reserva>();
        try {
            conn = this.conexionTransaccional != null ? this.conexionTransaccional : Conexion.getConnection();
            
            String query = this.addFilters(SQL_LIST_ALL, filter);    
            System.out.println("Ejecutando Query: " + query);
            stmt = conn.prepareStatement(query);            
            
            rs = stmt.executeQuery();
            while (rs.next()) {
                int codigo = rs.getInt("codigo");
                Date fecha = rs.getDate("fecha");
                Date fechaEntrada = rs.getDate("fecha_entrada");
                Date fechaSalida = rs.getDate("fecha_salida");
                int cantidaPersonas = rs.getInt("cantidad_personas");
                boolean estado = rs.getBoolean("estado");
                int documentoCliente = rs.getInt("documento_cliente");
                String idUsuario = rs.getString("id_usuario");

                reserva = new Reserva();
                reserva.setCodigo(codigo);
                reserva.setFecha(fecha);
                reserva.setFechaEntrada(fechaEntrada);
                reserva.setFechaSalida(fechaSalida);
                reserva.setCantidadPersonas(cantidaPersonas);
                reserva.setEstado(estado);
                Cliente cliente = getClientePorDocumento(documentoCliente);
                reserva.setCliente(cliente);
                Usuario usuario = getUsuarioPorId(idUsuario);
                reserva.setUsuario(usuario);

                reservas.add(reserva);
            }
        } finally {
            Conexion.close(rs);
            Conexion.close(stmt);
            if (this.conexionTransaccional == null) {
                Conexion.close(conn);
            }
        }

        return reservas;    
    }

   private String addFilters(String query, ReservaFilter filter){
       if(filter == null)
           return query;
       
       String filtrosWhere = "";
       
       if(filter.getCodigo()!= null){
           filtrosWhere += "codigo = " + filter.getCodigo();
       }
       
       if(filter.getFecha()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "fecha = " + filter.getFecha();
       }
       
       if(filter.getFechaEntrada()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "fecha_entrada = " + filter.getFechaEntrada();
       }
       
       if(filter.getFechaSalida()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "fecha_salida = " + filter.getFechaSalida();
       }
       
       if(filter.getCantidadPersonas()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "cantidad_personas = " + filter.getCantidadPersonas();
       }
       
       if(filter.getEstado() != null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "estado = " + filter.getEstado();
       }
       
       if(filter.getDocumentoCliente()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "documento_cliente = " + filter.getDocumentoCliente();
       }
       
       if(filter.getIdUsuario()!= null){
           if(!filtrosWhere.isEmpty())
               filtrosWhere += " AND ";
           filtrosWhere += "id_usuario = " + filter.getIdUsuario();
       }
       
       if(!filtrosWhere.isEmpty()){
           query = query + " WHERE " + filtrosWhere;
       }
       
       return query;
   }

    
    
}
