package capa_datos;

import capa_modelo.*;
import filter.UsuarioFilter;
import java.sql.SQLException;
import java.util.List;

public interface UsuarioDao {
    
    List<Usuario> listAll() throws SQLException;
    
    Usuario getUsuario(String id) throws SQLException;

    Integer insert(Usuario usuario) throws SQLException;
    
    Integer update(Usuario usuario) throws SQLException;
    
    Integer delete(Usuario usuario) throws SQLException;
    
    List<Usuario> filter(UsuarioFilter filter) throws SQLException;
}
