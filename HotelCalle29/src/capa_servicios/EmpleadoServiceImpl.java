package capa_servicios;

import capa_datos.EmpleadoJDBC;
import capa_modelo.Empleado;
import conexion.Conexion;
import filter.EmpleadoFilter;
import java.sql.SQLException;
import java.util.List;

public class EmpleadoServiceImpl implements EmpleadoService{
    
     private EmpleadoJDBC empleadoJdbc;

    public EmpleadoServiceImpl() {
        try {
            this.empleadoJdbc = new EmpleadoJDBC(Conexion.getConnection());
        } catch (Exception ex) {
            System.err.print("No se puede crear conexión: " + ex.getMessage());
        }
    }

    @Override
    public List<Empleado> listAll() throws SQLException {
        List<Empleado> listaEmpleados = empleadoJdbc.listAll();
        return (listaEmpleados);
    }

    @Override
    public Empleado getEmpleado(Integer documento) throws SQLException {
        Empleado obtenerEmpleado = empleadoJdbc.getEmpleado(documento);
        return (obtenerEmpleado);
    }

    @Override
    public Integer insert(Empleado empleado) throws SQLException {
        int insertarEmpleado = empleadoJdbc.insert(empleado);
        return(insertarEmpleado);
    }

    @Override
    public Integer update(Empleado empleado) throws SQLException {
        int actualizarEmpleado = empleadoJdbc.update(empleado);
        return(actualizarEmpleado);
    }

    @Override
    public Integer delete(Empleado empleado) throws SQLException {
        int eliminarEmpleado = empleadoJdbc.delete(empleado);
        return(eliminarEmpleado);
    }

    @Override
    public List<Empleado> filter(EmpleadoFilter filter) throws SQLException {
         List<Empleado> listaEmpleadosFiltrada = empleadoJdbc.filter(filter);
        return (listaEmpleadosFiltrada);
    }
    
}
