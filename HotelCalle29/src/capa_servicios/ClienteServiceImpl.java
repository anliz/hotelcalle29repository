package capa_servicios;

import capa_datos.ClienteJDBC;
import capa_modelo.Cliente;
import conexion.Conexion;
import filter.ClienteFilter;
import java.sql.SQLException;
import java.util.List;

public class ClienteServiceImpl implements ClienteService{
    
    private ClienteJDBC clienteJdbc;

    public ClienteServiceImpl() {
        try {
            this.clienteJdbc = new ClienteJDBC(Conexion.getConnection());
        } catch (Exception ex) {
            System.err.print("No se puede crear conexión: " + ex.getMessage());
        }
    }

    @Override
    public List<Cliente> listAll() throws SQLException {
        List<Cliente> listaClientes = clienteJdbc.listAll();
        return (listaClientes);
    }

    @Override
    public Cliente getCliente(Integer documento) throws SQLException {
        Cliente obtenerCliente = clienteJdbc.getCliente(documento);
        return(obtenerCliente);
    }

    @Override
    public Integer insert(Cliente cliente) throws SQLException {
        int insertarCliente = clienteJdbc.insert(cliente);
        return(insertarCliente);
    }

    @Override
    public Integer update(Cliente cliente) throws SQLException {
        int actualizarCliente = clienteJdbc.update(cliente);
        return(actualizarCliente);
    }

    @Override
    public Integer delete(Cliente cliente) throws SQLException {
        int eliminarCliente = clienteJdbc.delete(cliente);
        return(eliminarCliente);
    }

    @Override
    public List<Cliente> filter(ClienteFilter filter) throws SQLException {
         List<Cliente> listaClientesFiltrada = clienteJdbc.filter(filter);
        return (listaClientesFiltrada);
    }
    
}
