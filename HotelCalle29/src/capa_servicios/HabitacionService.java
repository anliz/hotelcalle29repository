/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capa_servicios;

import filter.HabitacionFilter;
import capa_modelo.Habitacion;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public interface HabitacionService {
    
    List<Habitacion> listAll() throws SQLException;
    
    Habitacion getHabitacion(Integer numero) throws SQLException;
    
    Integer insert(Habitacion habitacion) throws SQLException;
    
    Integer update(Habitacion habitacion) throws SQLException;
    
    Integer delete(Habitacion habitacion) throws SQLException;
    
    List<Habitacion> filter(HabitacionFilter filter) throws SQLException;
    
}
