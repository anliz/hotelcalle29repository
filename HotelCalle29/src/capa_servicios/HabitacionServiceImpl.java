/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package capa_servicios;

import conexion.Conexion;
import capa_datos.HabitacionJDBC;
import filter.HabitacionFilter;
import capa_modelo.Habitacion;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public class HabitacionServiceImpl implements HabitacionService {

    private HabitacionJDBC habitacionJdbc;

    public HabitacionServiceImpl() {
        try {
            this.habitacionJdbc = new HabitacionJDBC(Conexion.getConnection());
        } catch (Exception ex) {
            System.err.print("No se puede crear conexión: " + ex.getMessage());
        }

    }

    @Override
    public List<Habitacion> listAll() throws SQLException {
        List<Habitacion> listaHabitaciones = habitacionJdbc.listAll();
        return (listaHabitaciones);
    }

    @Override
    public Habitacion getHabitacion(Integer numero) throws SQLException {
        Habitacion obtenerHabitacion = habitacionJdbc.getHabitacion(numero);
        return (obtenerHabitacion);
    }

    @Override
    public Integer insert(Habitacion habitacion) throws SQLException {
       Integer insertarHabitacion = habitacionJdbc.insert(habitacion);
        return (insertarHabitacion);
    }

    @Override
    public Integer update(Habitacion habitacion) throws SQLException {
        int actualizarHabitacion = habitacionJdbc.update(habitacion);
        return (actualizarHabitacion);
    }

    @Override
    public Integer delete(Habitacion habitacion) throws SQLException {
        int eliminarHabitacion = habitacionJdbc.delete(habitacion);
        return (eliminarHabitacion);
    }

    @Override
    public List<Habitacion> filter(HabitacionFilter filter) throws SQLException {
        List<Habitacion> listaHabitacionesFiltrada = habitacionJdbc.filter(filter);
        return (listaHabitacionesFiltrada);
    }

}
