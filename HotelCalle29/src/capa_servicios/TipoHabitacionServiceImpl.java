package capa_servicios;

import capa_datos.TipoHabitacionJDBC;
import capa_modelo.TipoHabitacion;
import conexion.Conexion;
import filter.TipoHabitacionFilter;
import java.sql.SQLException;
import java.util.List;


public class TipoHabitacionServiceImpl implements TipoHabitacionService{
    
    private TipoHabitacionJDBC tipoHabitacionJdbc;
    
   public TipoHabitacionServiceImpl() {
        try {
            this.tipoHabitacionJdbc = new TipoHabitacionJDBC(Conexion.getConnection());
        } catch (Exception ex) {
            System.err.print("No se puede crear conexión: " + ex.getMessage());
        }
    }  

    @Override
    public List<TipoHabitacion> listAll() throws SQLException {
      List<TipoHabitacion> listaTipoHabitaciones = tipoHabitacionJdbc.listAll();
        return (listaTipoHabitaciones);
    }

    @Override
    public TipoHabitacion getTipoHabitacion(Integer codigo) throws SQLException {
        TipoHabitacion obtenerTipoHabitacion = tipoHabitacionJdbc.getTipoHabitacion(codigo);
        return(obtenerTipoHabitacion);
    }

    @Override
    public Integer insert(TipoHabitacion tipoHabitacion) throws SQLException {
        int insertarTipoHabitacion = tipoHabitacionJdbc.insert(tipoHabitacion);
        return(insertarTipoHabitacion);
    }

    @Override
    public Integer update(TipoHabitacion tipoHabitacion) throws SQLException {
        int actualizarTipoHabitacion = tipoHabitacionJdbc.update(tipoHabitacion);
        return(actualizarTipoHabitacion);
    }

    @Override
    public Integer delete(TipoHabitacion tipoHabitacion) throws SQLException {
        int eliminarTipoHabitacion = tipoHabitacionJdbc.delete(tipoHabitacion);
        return(eliminarTipoHabitacion);
    }

    @Override
    public List<TipoHabitacion> filter(TipoHabitacionFilter filter) throws SQLException {
       List<TipoHabitacion> listaTipoHabitacionesFiltrada = tipoHabitacionJdbc.filter(filter);
        return (listaTipoHabitacionesFiltrada);
    }
    
}
