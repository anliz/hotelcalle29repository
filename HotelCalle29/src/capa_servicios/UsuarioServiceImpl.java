package capa_servicios;

import capa_datos.UsuarioJDBC;
import capa_modelo.Habitacion;
import capa_modelo.Usuario;
import conexion.Conexion;
import filter.UsuarioFilter;
import java.sql.SQLException;
import java.util.List;

public class UsuarioServiceImpl implements UsuarioService {
    
    private UsuarioJDBC usuarioJdbc;

    public UsuarioServiceImpl() {
        try {
            this.usuarioJdbc = new UsuarioJDBC(Conexion.getConnection());
        } catch (Exception ex) {
            System.err.print("No se puede crear conexión: " + ex.getMessage());
        }
    }

    @Override
    public List<Usuario> listAll() throws SQLException {
        List<Usuario> listaUsuarios = usuarioJdbc.listAll();
        return (listaUsuarios);
    }

    @Override
    public Usuario getUsuario(String id) throws SQLException {
        Usuario obtenerUsuario = usuarioJdbc.getUsuario(id);
        return(obtenerUsuario);
    }

    @Override
    public Integer insert(Usuario usuario) throws SQLException {
        int insertarUsuario = usuarioJdbc.insert(usuario);
        return(insertarUsuario);
    }

    @Override
    public Integer update(Usuario usuario) throws SQLException {
        int actualizarUsuario = usuarioJdbc.update(usuario);
        return(actualizarUsuario);
    }

    @Override
    public Integer delete(Usuario usuario) throws SQLException {
        int eliminarUsuario = usuarioJdbc.delete(usuario);
        return(eliminarUsuario);
    }

    @Override
    public List<Usuario> filter(UsuarioFilter filter) throws SQLException {
         List<Usuario> listaUsuariosFiltrada = usuarioJdbc.filter(filter);
        return (listaUsuariosFiltrada);
    }
    
}
