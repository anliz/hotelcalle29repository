package capa_servicios;

import capa_datos.ReservaJDBC;
import capa_modelo.Reserva;
import conexion.Conexion;
import filter.ReservaFilter;
import java.sql.SQLException;
import java.util.List;

public class ReservaServiceImpl implements ReservaService{
    
    private static ReservaJDBC reservaJdbc;

    public ReservaServiceImpl() {
        try {
            this.reservaJdbc = new ReservaJDBC(Conexion.getConnection());
        } catch (Exception ex) {
            System.err.print("No se puede crear conexión: " + ex.getMessage());
        }
    }
    
    @Override
    public List<Reserva> listAll() throws SQLException {
        List<Reserva> listaReservas = reservaJdbc.listAll();
        return (listaReservas);
    }

    @Override
    public Reserva getReserva(Integer codigo) throws SQLException {
        Reserva obtenerReserva = reservaJdbc.getReserva(codigo);
        return(obtenerReserva);
    }

    @Override
    public Integer insert(Reserva reserva) throws SQLException {
        int insertarReserva = reservaJdbc.insert(reserva);
        return(insertarReserva);
    }

    @Override
    public Integer update(Reserva reserva) throws SQLException {
        int actualizarReserva = reservaJdbc.update(reserva);
        return(actualizarReserva);
    }

    @Override
    public Integer delete(Reserva reserva) throws SQLException {
        int eliminarReserva = reservaJdbc.delete(reserva);
        return(eliminarReserva);
    }

    @Override
    public List<Reserva> filter(ReservaFilter filter) throws SQLException {
        List<Reserva> listaReservaFiltrada = reservaJdbc.filter(filter);
        return(listaReservaFiltrada);
    }
    
    
    
}
